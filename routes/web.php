<?php
Route::get('/26f1608d4a6508339c400edc97b87a7b', function () {
    return view('admin/login');
});

Route::group(['middleware' => ['auth', 'revalidate', 'web']], function () {
    //////////PROFILE///////////////
    Route::get('/admin/profile', 'Admin\profile\ProfileController@index')->name('admin.profile');
    Route::get('/admin/profile/about-me', 'Admin\profile\ProfileController@aboutMe')->name('admin.profile.aboutme');
    Route::put('/admin/profile/about-me', 'Admin\profile\ProfileController@editAboutMe')->name('admin.profile.aboutme.edit');
    Route::get('/admin/profile/personal-info', 'Admin\profile\ProfileController@personalInfo')->name('admin.profile.personalInfo');
    Route::put('/admin/profile/personal-info', 'Admin\profile\ProfileController@editPersonalInfo')->name('admin.profile.personalInfo.edit');
    Route::get('/admin/profile/presentation', 'Admin\profile\ProfileController@presentation')->name('admin.profile.presentation');
    Route::put('/admin/profile/presentation', 'Admin\profile\ProfileController@editPresentation')->name('admin.profile.presentation.edit');

    ////////NETWORKTYPES
    Route::get('/admin/network-types', 'Admin\NetworkTypes\NetworkTypesController@index')->name('admin.network_types');
    Route::get('/admin/network-types/create', 'Admin\NetworkTypes\NetworkTypesController@create')->name('admin.network_types.create');
    Route::post('/admin/network-types/store', 'Admin\NetworkTypes\NetworkTypesController@store')->name('admin.network_types.store');
    Route::get('/admin/network-types/{id}/edit', 'Admin\NetworkTypes\NetworkTypesController@edit')->name('admin.network_types.edit');
    Route::put('/admin/network-types/update', 'Admin\NetworkTypes\NetworkTypesController@update')->name('admin.network_types.update');
    Route::delete('/admin/network-types/delete', 'Admin\NetworkTypes\NetworkTypesController@destroy')->name('admin.network_types.delete');

    ////////SKILLTYPES
    Route::get('/admin/type-skills', 'Admin\TypeSkills\TypeSkillsController@index')->name('admin.type_skills');
    Route::get('/admin/type-skills/create', 'Admin\TypeSkills\TypeSkillsController@create')->name('admin.type_skills.create');
    Route::post('/admin/type-skills/store', 'Admin\TypeSkills\TypeSkillsController@store')->name('admin.type_skills.store');
    Route::get('/admin/type-skills/{id}/edit', 'Admin\TypeSkills\TypeSkillsController@edit')->name('admin.type_skills.edit');
    Route::put('/admin/type-skills/update', 'Admin\TypeSkills\TypeSkillsController@update')->name('admin.type_skills.update');
    Route::delete('/admin/type-skills/delete', 'Admin\TypeSkills\TypeSkillsController@destroy')->name('admin.type_skills.delete');

    ///////////NETWORKS/////////////////////////////////////
    Route::get('/admin/networks', 'Admin\Networks\NetworksController@index')->name('admin.networks');
    Route::get('/admin/networks/create', 'Admin\Networks\NetworksController@create')->name('admin.networks.create');
    Route::post('/admin/networks/store', 'Admin\Networks\NetworksController@store')->name('admin.networks.store');
    Route::get('/admin/networks/{id}/edit', 'Admin\Networks\NetworksController@edit')->name('admin.networks.edit');
    Route::put('/admin/networks/update', 'Admin\Networks\NetworksController@update')->name('admin.networks.update');
    Route::delete('/admin/networks/delete', 'Admin\Networks\NetworksController@destroy')->name('admin.networks.delete');

    ///////////SKILLS
    Route::get('/admin/skills', 'Admin\Skills\SkillsController@index')->name('admin.skills');
    Route::get('/admin/skills/create', 'Admin\Skills\SkillsController@create')->name('admin.skills.create');
    Route::post('/admin/skills/store', 'Admin\Skills\SkillsController@store')->name('admin.skills.store');
    Route::get('/admin/skills/{id}/edit', 'Admin\Skills\SkillsController@edit')->name('admin.skills.edit');
    Route::put('/admin/skills/update', 'Admin\Skills\SkillsController@update')->name('admin.skills.update');
    Route::delete('/admin/skills/delete', 'Admin\Skills\SkillsController@destroy')->name('admin.skills.delete');

    ///////////EXPERIENCES
    Route::get('/admin/experiences', 'Admin\Experiences\ExperiencesController@index')->name('admin.experiences');
    Route::get('/admin/experiences/create', 'Admin\Experiences\ExperiencesController@create')->name('admin.experiences.create');
    Route::post('/admin/experiences/store', 'Admin\Experiences\ExperiencesController@store')->name('admin.experiences.store');
    Route::get('/admin/experiences/{id}/edit', 'Admin\Experiences\ExperiencesController@edit')->name('admin.experiences.edit');
    Route::put('/admin/experiences/update', 'Admin\Experiences\ExperiencesController@update')->name('admin.experiences.update');
    Route::delete('/admin/experiences/delete', 'Admin\Experiences\ExperiencesController@destroy')->name('admin.experiences.delete');

    //PROJECTS
    Route::get('/admin/projects', 'Admin\Projects\ProjectsController@index')->name('admin.projects');
    Route::get('/admin/projects/create', 'Admin\Projects\ProjectsController@create')->name('admin.projects.create');
    Route::post('/admin/projects/store', 'Admin\Projects\ProjectsController@store')->name('admin.projects.store');
    Route::get('/admin/projects/{id}/edit', 'Admin\Projects\ProjectsController@edit')->name('admin.projects.edit');
    Route::delete('/admin/projects/delete', 'Admin\Projects\ProjectsController@destroy')->name('admin.projects.delete');
    Route::put('/admin/projects/update', 'Admin\Projects\ProjectsController@update')->name('admin.projects.update');
    Route::get('/admin/example', 'Admin\Projects\ProjectsController@example')->name('admin.example');

    //SECTIONS PROJECT
    Route::get('/admin/{id}/project-sections', 'Admin\Projects\ProjectSectionsController@index')->name('admin.project_sections');
    Route::get('/admin/{id}/project-sections/create', 'Admin\Projects\ProjectSectionsController@create')->name('admin.project_sections.create');
    Route::post('/admin/project-sections/store', 'Admin\Projects\ProjectSectionsController@store')->name('admin.project_sections.store');
    Route::get('/admin/project-sections/{id}/edit', 'Admin\Projects\ProjectSectionsController@edit')->name('admin.project_sections.edit');
    Route::put('/admin/project-sections/update', 'Admin\Projects\ProjectSectionsController@update')->name('admin.project_sections.update');
    Route::delete('/admin/project-sections/delete', 'Admin\Projects\ProjectSectionsController@destroy')->name('admin.project_sections.delete');

    //BLOG    
    Route::get('/admin/blog', 'Admin\Blog\BlogController@index')->name('admin.blog');
    Route::get('/admin/blog/create', 'Admin\Blog\BlogController@create')->name('admin.blog.create');
    Route::post('/admin/blog/store', 'Admin\Blog\BlogController@store')->name('admin.blog.store');
    Route::get('/admin/blog/{id}/preview', 'Admin\Blog\BlogController@preview')->name('admin.blog.preview');
    Route::get('/admin/blog/{id}/edit', 'Admin\Blog\BlogController@edit')->name('admin.blog.edit');
    Route::put('/admin/blog/update', 'Admin\Blog\BlogController@update')->name('admin.blog.update');
    Route::delete('/admin/blog/delete', 'Admin\Blog\BlogController@destroy')->name('admin.blog.delete');

    //BLOG CATEGORIES
    Route::get('/admin/blog-categories', 'Admin\Blog\BlogCategoriesController@index')->name('admin.blog_categories');
    Route::get('/admin/blog-categories/create', 'Admin\Blog\BlogCategoriesController@create')->name('admin.blog_categories.create');
    Route::post('/admin/blog-categories/store', 'Admin\Blog\BlogCategoriesController@store')->name('admin.blog_categories.store');
    Route::get('/admin/blog-categories/{id}/edit', 'Admin\Blog\BlogCategoriesController@edit')->name('admin.blog_categories.edit');
    Route::put('/admin/blog-categories/update', 'Admin\Blog\BlogCategoriesController@update')->name('admin.blog_categories.update');
    Route::delete('/admin/blog-categories/delete', 'Admin\Blog\BlogCategoriesController@destroy')->name('admin.blog_categories.delete');

    //COURSES
    Route::get('/admin/courses', 'Admin\Courses\CoursesController@index')->name('admin.courses');
    Route::get('/admin/courses/create', 'Admin\Courses\CoursesController@create')->name('admin.courses.create');
    Route::post('/admin/courses/store', 'Admin\Courses\CoursesController@store')->name('admin.courses.store');
    Route::get('/admin/courses/{id}/edit', 'Admin\Courses\CoursesController@edit')->name('admin.courses.edit');
    Route::put('/admin/courses/update', 'Admin\Courses\CoursesController@update')->name('admin.courses.update');
    Route::delete('/admin/courses/delete', 'Admin\Courses\CoursesController@destroy')->name('admin.courses.delete');
    Route::get('/admin/courses/{id}/show', 'Admin\Courses\CoursesController@show')->name('admin.courses.show');

    //Tutorials
    Route::get('/admin/tutorials', function () {
        return view('errors.development', ['button' => 'admin']);
    });
});

Route::group(['middleware' => ['web', 'revalidate']], function () {

    Route::get('/admin', 'Admin\Auth\AuthController@index')->name('admin');
    Route::get('/', function () {
        return redirect('/@lurhx0');
    });
    Route::post('/admin/login', 'Admin\Auth\AuthController@login')->name('login');
    Route::get('/admin/logout', 'Admin\Auth\AuthController@logout')->name('logout');

    /////REGISTER
    Route::get('/register', 'Accesible\Register\RegisterController@index')->name('register');
    Route::get('/register/{token}/activate', 'Accesible\Register\RegisterController@activateAccount')->name('register.activate');
    Route::post('/register/done', 'Accesible\Register\RegisterController@register')->name('register.done');

    //////////PROFILE///////////////
    Route::get('/{user}/profile', 'Accesible\profile\ProfileController@index')->name('public.profile');
    Route::get('/{user}/', 'Accesible\profile\ProfileController@soRedirect')->name('public.so_redirect');

    //PROJECTS
    Route::get('/{user}/projects', 'Accesible\Projects\ProjectsController@index')->name('public.projects');

    //SECTIONS PROJECT
    Route::get('/{user}/{id}/project-sections', 'Accesible\Projects\ProjectSectionsController@index')->name('public.project_sections');

    //BLOG    
    Route::get('/{user}/blog', 'Accesible\Blog\BlogController@index')->name('public.blog');
    Route::get('/{user}/blog/{id}/view', 'Accesible\Blog\BlogController@view')->name('public.blog.view');

    //COURSES
    Route::get('/{user}/courses', 'Accesible\Courses\CoursesController@index')->name('public.courses');
    Route::get('/{user}/courses/{id}/show', 'Accesible\Courses\CoursesController@show')->name('public.courses.show');

    Route::get('/{user}/tutorials', function ($user) {
        return view('errors.development', ['button' => $user]);
    });
});


