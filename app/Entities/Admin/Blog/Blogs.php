<?php

namespace App\Entities\Admin\Blog;

use Illuminate\Database\Eloquent\Model;

class Blogs extends Model
{
    protected $table ='blogs';
    protected $fillable=['title','content','url_cover','description','id_categorie','id_user'];
}
