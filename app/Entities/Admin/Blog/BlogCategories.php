<?php

namespace App\Entities\Admin\Blog;

use Illuminate\Database\Eloquent\Model;

class BlogCategories extends Model
{
    protected $table ='blog_categories';
    protected $fillable=['name'];
}
