<?php

namespace App\Entities\Admin\Experiences;

use Illuminate\Database\Eloquent\Model;

class Experiences extends Model
{
    protected $table ='experiences';
    protected $fillable=['company','initial_date','final_date','about','id_user'];
}
