<?php

namespace App\Entities\Admin\ActivationTokens;

use Illuminate\Database\Eloquent\Model;

class ActivationTokens extends Model
{
    protected $table ='activate_tokens';
    protected $fillable=['token','id_user'];
}
