<?php

namespace App\Entities\Admin\Skills;

use Illuminate\Database\Eloquent\Model;

class Skills extends Model
{
    protected $table ='skills';
    protected $fillable=['name','id_type_skill','id_user'];
}
