<?php

namespace App\Entities\Admin\Projects;

use Illuminate\Database\Eloquent\Model;

class ProjectSections extends Model{
    protected $table ='project_sections';
    protected $fillable=['title','description','picture','id_project'];
}