<?php

namespace App\Entities\Admin\Projects;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    protected $table ='projects';
    protected $fillable=['name','description','link','id_user'];
}