<?php

namespace App\Entities\Admin\Courses;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $table ='courses';
    protected $fillable=['name','institute_name','file_type','file_url','license','id_user'];
}
