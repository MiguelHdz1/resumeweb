<?php

namespace App\Entities\Admin\Users;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{
    protected $table ='profiles';
    protected $fillable=['name','first_name','last_name','gender','birthday','phone_number',
        'avatar','background','about_me','profession','current_job','lives_in'];
}
