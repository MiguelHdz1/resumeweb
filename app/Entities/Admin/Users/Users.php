<?php

namespace App\Entities\Admin\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use \Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class Users extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract{
    use Authenticatable,
        Authorizable,
        CanResetPassword;

    protected $table='users';
    protected $fillable =['email','password','status'];
}
