<?php

namespace App\Entities\Admin\Networks;

use Illuminate\Database\Eloquent\Model;

class Networks extends Model
{
    protected $table ='networks';
    protected $fillable=['url','id_type_network','id_user'];
}
