<?php

namespace App\Entities\Admin\Networks;

use Illuminate\Database\Eloquent\Model;

class NetworkTypes extends Model
{
    protected $table ='network_types';
    protected $fillable=['name','icon','status'];
}
