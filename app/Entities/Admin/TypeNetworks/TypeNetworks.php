<?php

namespace App\Entities\Admin\TypeNetworks;

use Illuminate\Database\Eloquent\Model;

class TypeNetworks extends Model
{
    protected $table ='network_types';
    protected $fillable=['name','icon','status'];
}
