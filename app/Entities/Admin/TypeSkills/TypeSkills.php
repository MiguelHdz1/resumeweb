<?php

namespace App\Entities\Admin\TypeSkills;

use Illuminate\Database\Eloquent\Model;

class TypeSkills extends Model
{
    protected $table ='type_skills';
    protected $fillable=['name','icon','status'];
}
