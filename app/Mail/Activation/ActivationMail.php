<?php

namespace App\Mail\Activation;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $userName='';
    public $token='';
    
    public function __construct($userName,$token)
    {
        $this->userName = $userName;
        $this->token = $token;
    }

    public function build()
    {
        return $this->view('admin.mailable.activation')->with(['user'=> $this->userName,'token'=>$this->token])->subject('Activar cuenta');
    }
}