<?php

namespace App\Http\Controllers\Admin\TypeSkills;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Admin\TypeSkills\TypeSkills;
use App\Http\Requests\Admin\TypeSkills\TypeSkillsRequest;

class TypeSkillsController extends Controller {

    public function index(Request $request) {
        $button = false;
        $search = $request->input('search');
        $typeSkills = TypeSkills::select('name', 'icon', 'id')->where('status', '!=', '0');
        if ($search != '') {
            $button = true;
            $typeSkills->where('name', 'like', '%' . $search . '%');
        }

        return view('admin/typeSkills/index', ['typeSkills' => 
            $typeSkills->orderBy('created_at', 'desc')->paginate(6)->appends(['search' => $search]),
            'button' => $button, 'search' => $search]);
    }

    public function create() {
        return view('admin/typeSkills/create');
    }

    public function store(TypeSkillsRequest $request) {
        $typeSkill = new TypeSkills();
        $typeSkill->name = ucfirst(trim($request->name));
        $typeSkill->icon = strtolower(trim($request->icon));
        $typeSkill->status = '1';
        $typeSkill->save();
        return redirect('/admin/type-skills')->with('message', 'Habilidad registrada correctamente')
                        ->with('type', 'success-msg');
    }

    public function edit($id) {
        $typeSkill = TypeSkills::select('name', 'icon', 'id')->where('status', '!=', '0')->findOrFail($id);
        return view('admin/typeSkills/edit', ['typeSkill' => $typeSkill]);
    }

    public function update(TypeSkillsRequest $request) {
        $typeSkill = TypeSkills::where('status', '!=', '0')->findOrFail($request->id);
        $typeSkill->name = ucfirst(trim($request->name));
        $typeSkill->icon = strtolower(trim($request->icon));
        $typeSkill->save();
        return redirect('/admin/type-skills')->with('message', 'Habilidad editada correctamente')
                        ->with('type', 'success-msg');
    }

    public function destroy(Request $request) {
        $typeSkill = TypeSkills::where('status', '!=', '0')->findOrFail($request->id_delete);
        $typeSkill->status = '0';
        $typeSkill->save();
        return redirect('/admin/type-skills')->with('message', 'Habilidad eliminada correctamente')
                        ->with('type', 'success-msg');
    }

}
