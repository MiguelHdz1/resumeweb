<?php

namespace App\Http\Controllers\Admin\Networks;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Admin\Networks\Networks;
use App\Entities\Admin\Networks\NetworkTypes;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\Networks\NetworksRequest;

class NetworksController extends Controller
{
    
    public function index(Request $request)
    {
        $button=false;
        $search = $request->input('search');
        $networks = Networks::select('networks.id as id',\DB::raw('CONCAT(SUBSTRING(url,1,14),"...") as url'),'name', 'url as full_url')->where('status','!=','0')
                ->where('id_user','=',Auth::user()->id)
                ->join('network_types','network_types.id','=','id_type_network');           
        if($search!=''){
            $button=true;
            $networks->where('name','like','%'.$search.'%');
        }
        return view('admin/networks/index',['networks'=>$networks->orderBy('networks.created_at','desc')->paginate(6)->appends(['search' => $search]),
            'button'=>$button,'search'=>$search]);
    }

    public function create(){
        $networkTypes = NetworkTypes::select('id','name')->where('status','!=','0')->orderBy('name','asc');
        $network = Networks::where('id_user','=', Auth::user()->id)->count();
        if($network > $networkTypes->count()){
            return redirect('/admin/networks')->with('type','success-msg')
                    ->with('message','Haz alcanzado el limite de redes');
        }
        return view('admin/networks/create',['networkTypes'=>$networkTypes->get()]);
    }

    public function store(NetworksRequest $request){
        $networks = new Networks();
        $networks->url = trim($request->url);
        $networks->id_type_network= trim($request->id_type);
        $networks->id_user= Auth::user()->id;
        $networks->save();
        return redirect('/admin/networks')->with('type','success-msg')
                    ->with('message','Red agregada a tu perfil');
    }

    public function edit($id){
        $network = Networks::select('id','url','id_type_network')->where('id_user','=',Auth::user()->id)->findOrFail($id);
        $networkTypes = NetworkTypes::select('id','name')->where('status','!=','0')->orderBy('name','asc')->get();        
        return view('admin/networks/edit',['networkTypes'=>$networkTypes,'network'=>$network]);
    }

    public function update(NetworksRequest $request){
        $network = Networks::where('id_user','=',Auth::user()->id)->findOrFail($request->id);
        $network->url = trim($request->url);
        $network->id_type_network= trim($request->id_type);
        $network->save();
        return redirect('/admin/networks')->with('type','success-msg')
                    ->with('message','Red editada con exito');
    }

    public function destroy(Request $request){
        $network = Networks::where('id_user','=',Auth::user()->id)->findOrFail($request->id_delete);
        $network->delete();
        return redirect('/admin/networks')->with('type','success-msg')
                    ->with('message','La red se ha eliminado con exito');
    }
}
