<?php

namespace App\Http\Controllers\Admin\NetworkTypes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Admin\Networks\NetworkTypes;
use App\Http\Requests\Admin\NetworkTypes\NetworkTypeRequest;
use App\Http\Requests\Admin\NetworkTypes\NetworkTypeEditRequest;

class NetworkTypesController extends Controller {

    public function index(Request $request) {
        $button = false;
        $search = $request->input('search');
        $networks = NetworkTypes::select('id', 'name', 'icon', 'status')->where('status', '!=', '0');
        if ($search != '') {
            $button = true;
            $networks->where('name', 'like', '%' . $search . '%');
        }
        return view('admin/networkTypes/index', ['networks' => $networks->orderBy('created_at', 'desc')->paginate(6)->appends(['search' => $search]),
            'button' => $button, 'search' => $search]);
    }

    public function create() {
        return view('admin/networkTypes/create');
    }

    public function store(NetworkTypeRequest $request) {
        $name = ucfirst(trim($request->name));
        $extension = $request->icon->clientExtension();
        $path = 'images/network/icons';
        $networkType = new NetworkTypes();
        $networkType->name = $name;
        $networkType->icon = $path . '/' . strtolower($name) . '.' . $extension;
        $networkType->status = '1';
        $request->icon->move(public_path($path), strtolower($name) . '.' . $extension);
        $networkType->save();
        return redirect('/admin/network-types')->with('message', 'Red registrada correctamente')
                        ->with('type', 'success-msg');
    }

    public function edit($id) {
        $network = NetworkTypes::select('id', 'name', \DB::raw("SUBSTRING_INDEX(icon,'/',-1) as icon"))->where('status', '!=', '0')
                ->findOrFail($id);
        return view('admin/networkTypes/edit', ['network' => $network]);
    }

    public function update(NetworkTypeEditRequest $request) {
        $network = NetworkTypes::where('status', '!=', '0')
                ->findOrFail($request->id);
        $name = ucfirst(trim($request->name));
        $network->name = $name;
        if ($request->icon != null) {
            $extension = $request->icon->clientExtension();
            $path = 'images/network/icons';
            if (file_exists(public_path($network->icon))) {
                if ($network->icon != '') {
                    unlink(public_path($network->icon));
                }
            }
            $network->icon = $path . '/' . strtolower($name) . '.' . $extension;
            $request->icon->move(public_path($path), strtolower($name) . '.' . $extension);
        }
        $network->save();
        return redirect('/admin/network-types')->with('message', 'Red editada correctamente')
                        ->with('type', 'success-msg');
    }

    public function destroy(Request $request) {
        $network = NetworkTypes::where('status', '!=', '0')
                ->findOrFail($request->id_delete);
        $network->status = '0';
        $network->save();
        return redirect('/admin/network-types')->with('message', 'Red eliminada correctamente')
                        ->with('type', 'success-msg');
    }

}
