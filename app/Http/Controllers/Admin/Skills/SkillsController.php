<?php

namespace App\Http\Controllers\Admin\Skills;

use Illuminate\Http\Request;
use App\Entities\Admin\Skills\Skills;
use App\Entities\Admin\TypeSkills\TypeSkills;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Skills\SkillsRequest;
use Illuminate\Support\Facades\Auth;

class SkillsController extends Controller {

    public function index(Request $request) {
        $button = false;
        $search = $request->input('search');
        $skills = Skills::select('skills.id as id', 'skills.name as name', 'type_skills.name as type')
                        ->join('type_skills', 'id_type_skill', '=', 'type_skills.id')
                        ->where('id_user', '=', Auth::user()->id);
        if ($search != '') {
            $button = true;
            $skills->where('skills.name', 'like', '%' . $search . '%')->OrWhere('type_skills.name', 'like', '%' . $search . '%');
        }
        return view('admin/skills/index', ['button' => $button, 'search' => $search,
            'skills' => $skills->orderBy('skills.created_at', 'desc')->paginate(6)->appends(['search' => $search])]);
    }

    public function create() {
        if (Skills::where('id', '=', Auth::user()->id)->count() <=100) {
            $typeSkills = TypeSkills::select('id', 'name')->where('status', '!=', '0')->orderBy('name','asc')->get();
            return view('admin/skills/create',['typeSkills'=>$typeSkills]);
        }else{
            return redirect('/admin/skills')->with('type','error-msg')
                    ->with('message','Haz alcanzado el número maximo de habilidades');
        }
    }

    public function store(SkillsRequest $request) {
        $skill = new Skills();
        $skill->name= ucfirst(trim($request->name));
        $skill->id_type_skill= trim($request->type_skill);
        $skill->id_user=Auth::user()->id;
        $skill->save();
        return redirect('/admin/skills')->with('type','success-msg')
                    ->with('message','Habilidad agregada a tu perfil');
    }
    
    public function edit($id) {
        $skill = Skills::select('id','name','id_type_skill')->where('id_user','=',Auth::user()->id)->findOrFail($id);
        $typeSkills = TypeSkills::select('id', 'name')->where('status', '!=', '0')->orderBy('name','asc')->get();
        return view('admin/skills/edit',['skill'=>$skill,'typeSkills'=>$typeSkills]);
    }

    public function update(SkillsRequest $request) {
        $skill = Skills::where('id_user','=',Auth::user()->id)->findOrFail($request->id);
        $skill->name= ucfirst(trim($request->name));
        $skill->id_type_skill= trim($request->type_skill);
        $skill->save();
        return redirect('/admin/skills')->with('type','success-msg')
                    ->with('message','La habilidad ha sido editada con exito');
    }

    public function destroy(Request $request) {
        $skill = Skills::where('id_user','=',Auth::user()->id)->findOrFail($request->id_delete);
        $skill->delete();
        return redirect('/admin/skills')->with('type','success-msg')
                    ->with('message','La habilidad se ha eliminado con exito');
    }

}
