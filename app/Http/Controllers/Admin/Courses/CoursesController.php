<?php

namespace App\Http\Controllers\Admin\Courses;

use App\Entities\Admin\Courses\Courses;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\Courses\CoursesRequest;
use App\Http\Requests\Admin\Courses\CoursEditRequest;

class CoursesController extends Controller {

    public function index(Request $request) {
        $colors = ['light-blue darken-3', 'cyan darken-3', 'teal darken-3',
            'blue darken-3', 'indigo darken-2', 'red darken-3', 'yellow darken-3',
            'amber darken-3', 'orange darken-3', 'green darken-3', 'light-green darken-3',
            'amber accent-4', 'blue-grey darken-3', 'green darken-1'];
        $button = false;
        $search = $request->input('search');
        $courses = Courses::select('id', 'name', 'institute_name', 'file_type', 'file_url', 'license')
                ->where('id_user', '=', Auth::user()->id);
        if ($search) {
            $button = true;
            $courses->where('name', 'like', '%' . $search . '%');
        }

        return view('admin.courses.index', ['courses' => $courses->orderBy('created_at', 'desc')->paginate(6)->appends(['search' => $search])
            , 'colors' => $colors, 'button' => $button, 'search' => $search]);
    }

    public function create() {
        return view('admin.courses.create');
    }

    public function store(CoursesRequest $request) {
        $course = new Courses();
        $course->name = trim($request->name);
        $course->institute_name = trim($request->institution);
        $course->description = trim($request->description);
        $course->file_type = trim($request->type_course);
        if(preg_replace('/\s+/', '', $request->license) != ''){
           $course->license = trim($request->license);
        }
        $course->id_user = Auth::user()->id;
        $course->save();
        $img = Courses::findOrFail($course->id);
        $extension = $request->file_course->clientExtension();
        $path = 'files/users/' . Auth::user()->id . '/courses';
        if (file_exists(public_path($path . '/' . $img->id . '.' . $extension))) {
            unlink(public_path($path . '/' . $img->id . '.' . $extension));
        }
        $img->file_url = $path . '/' . $img->id . '.' . $extension;
        $request->file_course->move(public_path($path), $img->id . '.' . $extension);
        $img->save();
        return redirect('/admin/courses')->with('type', 'success-msg')
                        ->with('message', 'El curso se ha gregado con exito');
    }

    public function show($id) {
        $course = Courses::select('name', 'institute_name', 'license',
                'file_type', 'description','file_url')
                        ->where('id_user', '=', Auth::user()->id)->findOrFail($id);
        return view('admin.courses.show',['course'=>$course]);
    }

    public function edit($id) {
        $course = Courses::select('id','name', 'institute_name', 'license', 'file_type',
                'description', \DB::raw("SUBSTRING_INDEX(file_url,'/',-1) as file_url"))
                        ->where('id_user', '=', Auth::user()->id)->findOrFail($id);
        return view('admin.courses.edit', ['course' => $course]);
    }

    public function update(CoursEditRequest $request) {
        $course = Courses::where('id_user', '=', Auth::user()->id)->findOrFail($request->id);
        $course->name = trim($request->name);
        $course->institute_name = trim($request->institution);
        $course->description = trim($request->description);
        $course->file_type = trim($request->type_course);
        if(preg_replace('/\s+/', '', $request->license) != ''){
           $course->license = trim($request->license);
        }else{
            $course->license = '';
        }
        if ($request->file_course != null) {
            $extension = $request->file_course->clientExtension();
            $path = 'files/users/' . Auth::user()->id . '/courses';
            if (file_exists(public_path($course->file_url))) {
                unlink(public_path($course->file_url));
            }
            $course->file_url = $path . '/' . $course->id . '.' . $extension;
            $request->file_course->move(public_path($path), $course->id . '.' . $extension);
        }
        $course->save();
        return redirect('/admin/courses')->with('type', 'success-msg')
                        ->with('message', 'El curso se ha editado con exito');
    }

    public function destroy(Request $request) {
        $course = Courses::where('id_user', '=', Auth::user()->id)->findOrFail($request->id_delete);
        if (file_exists(public_path($course->file_url))) {
                unlink(public_path($course->file_url));
            }
        $course->delete();
        return redirect('/admin/courses')->with('type', 'success-msg')
                        ->with('message', 'El curso se ha eliminado con exito');
    }

}
