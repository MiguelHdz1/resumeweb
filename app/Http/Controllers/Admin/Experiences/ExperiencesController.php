<?php

namespace App\Http\Controllers\Admin\Experiences;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Admin\Experiences\Experiences;
use App\Http\Requests\Admin\Experiences\ExperiencesRequest;

class ExperiencesController extends Controller {

    public function index(Request $request) {
        $button = false;
        $search = $request->input('search');
        $experiences = Experiences::select('id', 'company', \DB::raw('DATE_FORMAT(initial_date,"%d/%m/%Y") as initial_date'), \DB::raw('DATE_FORMAT(final_date,"%d/%m/%Y") as final_date'), 'current')->where('id_user', '=', \Auth::user()->id);
        if ($search != '') {
            $button = true;
            $experiences->where('company', 'like', '%' . $search . '%');
        }
        return view('admin/experiences/index', ['experiences' => $experiences->orderBy('current', 'desc')->orderBy('created_at', 'desc')->paginate(6)->appends(['search' => $search]),
            'button' => $button, 'search' => $search]);
    }

    public function create() {
        $withCurrent = Experiences::where('current', '=', '1')->where('id_user', '=', \Auth::user()->id)->count();
        $current = ($withCurrent > 0) ? true : false;
        return view('admin/experiences/create', ['current' => $current]);
    }

    public function store(ExperiencesRequest $request) {
        $experience = new Experiences();
        $experience->company = trim($request->company);
        $experience->initial_date = trim(date_format(date_create_from_format('d/m/Y', $request->initial_date), 'Y-m-d') . ' 00:00:00');
        $experience->final_date = trim(date_format(date_create_from_format('d/m/Y', $request->final_date), 'Y-m-d') . ' 00:00:00');
        $experience->about = trim($request->about);
        $experience->id_user = \Auth::user()->id;
        $withCurrent = Experiences::where('current', '=', '1')->where('id_user', '=', \Auth::user()->id)->count();
        if ($withCurrent == 0) {
            if ($request->current != null) {
                $experience->current = '1';
            } else {
                $experience->current = '0';
            }
        }else{
            $experience->current = '0';
        }
        $experience->save();
        return redirect('/admin/experiences')->with('message', 'Experiencia agregada a tu perfil')
                        ->with('type', 'success-msg');
    }

    public function edit($id) {
        $experience = Experiences::select('id','company','current','about',
                \DB::raw('DATE_FORMAT(initial_date,"%d/%m/%Y") as initial_date'),
                \DB::raw('DATE_FORMAT(final_date,"%d/%m/%Y") as final_date'))
                ->where('id_user', '=', \Auth::user()->id);
        $withCurrent = Experiences::where('current', '=', '1')->where('id_user', '=', \Auth::user()->id)->count();
        $current = ($withCurrent > 0) ? true : false;
        return view('admin/experiences/edit', ['experience' => $experience->findOrFail($id),'current'=>$current]);
    }

    public function update(ExperiencesRequest $request) {
        $experience = Experiences::where('id_user', '=', \Auth::user()->id)->findOrFail($request->id);
        $experience->company = trim($request->company);
        $experience->initial_date = trim(date_format(date_create_from_format('d/m/Y', $request->initial_date), 'Y-m-d') . ' 00:00:00');
        $experience->final_date = trim(date_format(date_create_from_format('d/m/Y', $request->final_date), 'Y-m-d') . ' 00:00:00');
        $experience->about = trim($request->about);        
        $withCurrent = Experiences::where('current', '=', '1')->where('id_user', '=', \Auth::user()->id)->count();
        if ($withCurrent == 0) {
            if ($request->current != null) {
                $experience->current = '1';
            } else {
                $experience->current = '0';
            }
        }else{
            $experience->current = '0';
        }
        $experience->save();
        return redirect('/admin/experiences')->with('message', 'Experiencia editada correctamente')
                        ->with('type', 'success-msg');
    }

    public function destroy(Request $request) {
        $network = Experiences::where('id_user', '=', \Auth::user()->id)
                ->findOrFail($request->id_delete);
        $network->delete();
        return redirect('/admin/experiences')->with('message', 'Experiencia eliminada correctamente')
                        ->with('type', 'success-msg');
    }

}
