<?php

namespace App\Http\Controllers\Admin\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Admin\Blog\BlogCategories;
use App\Http\Requests\Admin\Blog\BlogCategorieRequest;

class BlogCategoriesController extends Controller {

    public function index(Request $request) {
        $button = false;
        $search = $request->input('search');
        $categories = BlogCategories::select('id', 'name')->where('status', '!=','0');
        if ($search != '') {
            $button = true;
            $categories->where('name', 'like', '%' . $search . '%');
        }
        return view('admin.blogCategories.index', ['categories' => $categories->paginate(6)->appends(['search' => $search]), 'button' => $button, 'search' => $search]);
    }

    public function create() {
        return view('admin.blogCategories.create');
    }

    public function store(BlogCategorieRequest $request) {
        $categorie = new BlogCategories();
        $categorie->name = trim($request->name);
        $categorie->status = '1';
        $categorie->save();
        return redirect('/admin/blog-categories')->with('message', 'Categoría creada correctamente')
                        ->with('type', 'success-msg');
    }

    public function edit($id) {
        $categorie = BlogCategories::select('id','name')->where('status', '!=','0')->findOrFail($id);        
        return view('admin.blogCategories.edit', ['categorie' => $categorie]);
    }

    public function update(BlogCategorieRequest $request) {
        $categorie = BlogCategories::where('status', '!=','0')->findOrFail($request->id);   
        $categorie->name = trim($request->name);
        $categorie->status = '1';
        $categorie->save();        
        return redirect('/admin/blog-categories')->with('message', 'Categoría editada correctamente')
                        ->with('type', 'success-msg');
    }

    public function destroy(Request $request) {
        $categorie = BlogCategories::where('status', '!=','0')->findOrFail($request->id_delete);   
        $categorie->status='0';  
        $categorie->save();  
        return redirect('/admin/blog-categories')->with('message', 'Categoría eliminada correctamente')
                        ->with('type', 'success-msg');
    }

}
