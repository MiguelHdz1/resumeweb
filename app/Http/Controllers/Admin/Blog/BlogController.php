<?php

namespace App\Http\Controllers\Admin\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Admin\Blog\BlogCategories;
use App\Entities\Admin\Blog\Blogs;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\Blog\BlogsRequest;

class BlogController extends Controller {

    public function index(Request $request) {
        $button = false;
        $search = $request->input('search');
        $blogs = Blogs::select('id', 'title', 'url_cover', 'description', \DB::raw('DATE_FORMAT(created_at,"%d/%m/%Y") as date'))
                ->where('id_user', '=', Auth::user()->id);
        if ($search != '') {
            $button = true;
            $blogs->where('title', 'like', '%' . $search . '%');
        }
        return view('admin.blogs.index', ['blogs' => $blogs->orderBy('created_at', 'desc')->paginate(6)->appends(['search' => $search]), 'button' => $button, 'search' => $search]);
    }

    public function create() {
        $categories = BlogCategories::select('id', 'name')->where('status', '!=', '0')->get();
        return view('admin.blogs.create', ['categories' => $categories]);
    }

    public function store(BlogsRequest $request) {
        $blog = new Blogs();
        $blog->title = trim($request->title);
        $blog->content = trim($request->blog_area);
        $blog->id_categorie = trim($request->categorie);
        $blog->description = trim($request->description);
        $blog->id_user = Auth::user()->id;
        if (trim($request->picture) != null) {
            $blog->save();
            $img = Blogs::where('id_user', '=', Auth::user()->id)->findOrFail($blog->id);
            $extension = $request->picture->clientExtension();
            $path = 'images/users/' . Auth::user()->id . '/blogs';
            if (file_exists(public_path($path . '/' . 'bl' . $img->id . $extension))) {
                unlink(public_path($path . '/' . 'bl' . $img->id . $extension));
            }
            $img->url_cover = $path . '/' . 'bl' . $img->id . '.' . $extension;
            $request->picture->move(public_path($path), 'bl' . $img->id . '.' . $extension);
            $img->save();
        } else {
            $blog->save();
        }
        return redirect('/admin/blog')->with('message', 'Blog creado correctamente')
                        ->with('type', 'success-msg');
    }

    public function preview($id) {
        $blog = Blogs::select('title', 'content', \DB::raw('DATE_FORMAT(created_at,"%d/%m/%Y") as date'), 'description', 'url_cover')
                        ->where('id_user', '=', Auth::user()->id)->findOrFail($id);
        return view('admin.blogs.preview', ['blog' => $blog]);
    }

    public function edit($id) {
        $blog = Blogs::select('id', 'title', 'id_categorie', 'url_cover', 'description', 'content', \DB::raw("SUBSTRING_INDEX(url_cover,'/',-1) as url_cover"))
                        ->where('id_user', '=', Auth::user()->id)->findOrFail($id);
        $categories = BlogCategories::select('id', 'name')->where('status', '!=', '0')->get();
        return view('admin.blogs.edit', ['blog' => $blog, 'categories' => $categories]);
    }

    public function update(BlogsRequest $request) {
        $blog = Blogs::where('id_user', '=', Auth::user()->id)->findOrFail($request->id);
        $blog->title = $request->title;
        $blog->content = $request->blog_area;
        $blog->id_categorie = $request->categorie;
        $blog->description = $request->description;
        $blog->id_user = Auth::user()->id;
        if (trim($request->picture) != null) {
            $extension = $request->picture->clientExtension();
            $path = 'images/users/' . Auth::user()->id . '/blogs';
            if (file_exists($blog->url_cover)) {
                if ($blog->url_cover != '') {
                    unlink(public_path($blog->url_cover));
                }
            }
            $blog->url_cover = $path . '/' . 'bl' . $blog->id . '.' . $extension;
            $request->picture->move(public_path($path), 'bl' . $blog->id . '.' . $extension);
        }
        $blog->save();
        return redirect('/admin/blog')->with('message', 'Blog editado correctamente')
                        ->with('type', 'success-msg');
    }

    public function destroy(Request $request) {
        $blog = Blogs::where('id_user', '=', Auth::user()->id)->findOrFail($request->id_delete);
        if (file_exists($blog->url_cover)) {
            if ($blog->url_cover != '') {
                unlink(public_path($blog->url_cover));
            }
        }
        $blog->delete();
        return redirect('/admin/blog')->with('message', 'Blog eliminado correctamente')
                        ->with('type', 'success-msg');
    }

}
