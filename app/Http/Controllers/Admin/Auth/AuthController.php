<?php
namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entities\Admin\Users\Profiles;

class AuthController extends Controller {

    public function index() {
        if (Auth::check()) {
            return redirect('admin/profile');
        } else {
            return redirect('/@lurhx0');
        }
    }

    public function login(Request $request) {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => !'0'])) {
            Auth::login(Auth::user(), true);        
            return redirect('/admin/profile')->with('message', 'Bienvenido '.Auth::user()->email)
                            ->with('type', 'success-msg');
        } else {
            return redirect('/26f1608d4a6508339c400edc97b87a7b')->with('message', 'Verifica tus credenciales')
                            ->with('type', 'error-msg');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
