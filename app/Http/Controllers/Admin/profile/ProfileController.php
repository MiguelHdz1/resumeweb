<?php

namespace App\Http\Controllers\Admin\profile;

use Illuminate\Http\Request;
use App\Entities\Admin\Experiences\Experiences;
use App\Http\Requests\Admin\Profile\AboutMeRequest;
use App\Http\Controllers\Controller;
use App\Entities\Admin\Users\Profiles;
use App\Entities\Admin\Users\Users;
use App\Entities\Admin\Networks\Networks;
use App\Entities\Admin\TypeSkills\TypeSkills;
use App\Http\Requests\Admin\Profile\PersonalInfoRequest;
use App\Http\Requests\Admin\Profile\PresentationRequest;
use App\Entities\Admin\Skills\Skills;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller {

    public function index() {
        $id = \Auth::user()->id;
        $profile = Users::select(\DB::raw('DATE_FORMAT(birthday,"%d/%m/%Y") as birthday'),'user_link', 'phone_number', 'about_me', 'profession', 'current_job', 'lives_in')
                ->where('users.id','=',$id)->leftJoin('profiles','profiles.id_user','=','users.id')->firstOrFail();        
        $type_skills = TypeSkills::select('id', 'name', 'icon')->where('status', '!=', '0')->get();
        $skills = array();
        foreach ($type_skills as $type_skill) {
            $skill = Skills::select('name')->where('id_type_skill', '=', $type_skill->id)->where('id_user', '=', $id);
            if ($skill->count() > 0) {
                $skills[] = array('name' => $type_skill->name, 'icon' => $type_skill->icon, 'current_skills' => $skill->get());
            }
        }
        $experiences = Experiences::select('company','about',\DB::raw('DATE_FORMAT(initial_date,"%d/%m/%Y") as initial_date'),
                \DB::raw('DATE_FORMAT(final_date,"%d/%m/%Y") as final_date'),'current')->where('id_user', '=', $id)
                ->orderBy('current', 'desc')->orderBy('created_at', 'desc')->get();
        $networks = Networks::select(\DB::raw('CONCAT(SUBSTRING(url,1,28),"...") as url'), 'url as full_url', 'name', 'icon', 'status')->where('status', '!=', '0')
                        ->where('id_user', '=', $id)->join('network_types', 'network_types.id', '=', 'networks.id_type_network')->get();
        return view('admin/profile/index', ['profile' => $profile,'experiences'=>$experiences, 'skills' => $skills, 'networks' => $networks]);
    }

    public function aboutMe() {
        $about = Profiles::select('id', 'about_me')->where('id_user', '=', Auth::user()->id)->firstOrFail();
        return view('admin/profile/edit_about', ['about' => $about]);
    }

    public function editAboutMe(AboutMeRequest $request) {
        $about = Profiles::select('id', 'about_me')->where('id_user', '=', Auth::user()->id)->findOrFail($request->id);
        $about->about_me = $request->about_me;
        $about->save();
        return redirect('/admin/profile')->with('type', 'success-msg')
                        ->with('message', 'Haz editado la información acerca de ti');
    }

    public function personalInfo(){
        $profile = Profiles::select('id','profession', 'current_job', \DB::raw('DATE_FORMAT(birthday,"%d/%m/%Y") as birthday'), 'lives_in')
                        ->where('id_user', '=', Auth::user()->id)->firstOrFail();
        return view('admin/profile/personal_info', ['profile' => $profile]);
    }

    public function editPersonalInfo(PersonalInfoRequest $request) {
        $profile = Profiles::where('id_user', '=', Auth::user()->id)->findOrFail($request->id);
        $profile->profession=trim(ucfirst($request->profession));
        $profile->current_job=trim(ucfirst($request->current_job));
        $profile->birthday=trim(date_format(date_create_from_format('d/m/Y', $request->birthday), 'Y-m-d').' 00:00:00');
        $profile->lives_in=trim(ucfirst($request->lives_in));
        $profile->save();
        return redirect('/admin/profile')->with('type', 'success-msg')
                        ->with('message', 'Haz editado la información personal');
    }

    public function presentation() {
        $profile = Profiles::select('name','first_name','last_name','phone_number','email',
                \DB::raw("SUBSTRING_INDEX(avatar,'/',-1) as avatar"),
                \DB::raw("SUBSTRING_INDEX(background,'/',-1) as background"))
                ->where('id_user','=',Auth::user()->id)
                ->where('status','!=','0')
                ->join('users','users.id','=','id_user')->firstOrFail();        
        return view('/admin/profile/presentation',['profile'=>$profile]);
    }
    
    public function editPresentation(PresentationRequest $request) {
        $user = Users::where('status','!=','0')->findOrFail(Auth::user()->id);
        $profile = Profiles::where('id_user','=',Auth::user()->id)->firstOrFail();        
        if(preg_replace('/\s+/', '', $request->password) != ''){
            $user->password = bcrypt(trim($request->password));
        }
        $profile->name = trim($request->name);
        $profile->first_name = trim($request->first_name);
        $profile->last_name = trim($request->last_name);
        $profile->phone_number = trim($request->phone);
        
        if (trim($request->file_profile) != null) {
            $extension = $request->file_profile->clientExtension();
            $path = 'images/users/'.Auth::user()->id.'/profile';
            if(file_exists(public_path($profile->avatar))){
                if($profile->avatar != ''){
                    unlink(public_path($profile->avatar));                
                }                
            }
            $profile->avatar = $path . '/' . strtolower('avatar_'.Auth::user()->id) . '.' . $extension;
            $request->file_profile->move(public_path($path), strtolower('avatar_'.Auth::user()->id) . '.' . $extension);
        }
        
        if ($request->file_back != null) {
            $extension = $request->file_back->clientExtension();
            $path = 'images/users/'.Auth::user()->id.'/profile';
            if(file_exists(public_path($profile->background))){
                if($profile->background != ''){
                    unlink(public_path($profile->background));
                }                              
            }
            $profile->background = $path . '/' . strtolower('background_'.Auth::user()->id) . '.' . $extension;
            $request->file_back->move(public_path($path), strtolower('background_'.Auth::user()->id) . '.' . $extension);
        }
        $user->save();
        $profile->save();
        return redirect('/admin/profile')->with('type', 'success-msg')
                        ->with('message', 'Haz editado tu presentación');
    }
}