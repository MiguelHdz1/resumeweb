<?php

namespace App\Http\Controllers\Admin\Projects;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Admin\Projects\ProjectSections;
use App\Entities\Admin\Projects\Projects;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\Projects\ProjectsRequest;

class ProjectsController extends Controller {

    public function index(Request $request) {
        $button = false;
        $search = $request->input('search');
        $projects = Projects::select('name', 'description', 'id', 'image_url')->where('id_user', '=', Auth::user()->id);
        if ($search != '') {
            $button = true;
            $projects->where('name', 'like', '%' . $search . '%');
        }
        return view('admin.projects.index', ['projects' => $projects->orderBy('created_at', 'desc')
                    ->paginate(6)->appends(['search' => $search]),
            'button' => $button, 'search' => $search]);
    }

    public function example(Request $request) {
        return view('admin.projects.example');
    }

    public function create() {
        return view('admin/projects/create');
    }

    public function store(ProjectsRequest $request) {

        $project = new Projects();
        $project->name = trim($request->name);
        $project->description = trim($request->description);
        if (preg_replace('/\s+/', '', trim($request->url)) != '') {
            $project->link = trim($request->url);
        }
        $project->id_user = Auth::user()->id;
        if (trim($request->image_project) != null) {
            $project->save();
            $img = Projects::where('id_user', '=', Auth::user()->id)->findOrFail($project->id);
            $extension = $request->image_project->clientExtension();
            $path = 'images/users/' . Auth::user()->id . '/projects';
            if (file_exists(public_path($path . '/' . 'pr' . $img->id . $extension))) {
                unlink(public_path($path . '/' . 'pr' . $img->id . $extension));
            }
            $img->image_url = $path . '/' . 'pr' . $img->id . '.' . $extension;
            $request->image_project->move(public_path($path), 'pr' . $img->id . '.' . $extension);
            $img->save();
        } else {
            $project->save();
        }
        return redirect('/admin/' . $project->id . '/project-sections')->with('type', 'success-msg')
                        ->with('message', 'El proyecto se ha agregado, ahora puedes agregar sus secciones');
    }

    public function edit($id) {
        $project = Projects::select('id', 'name', 'description', 'link', \DB::raw("SUBSTRING_INDEX(image_url,'/',-1) as image_url"))
                        ->where('id_user', '=', Auth::user()->id)->findOrFail($id);
        return view('admin.projects.edit', ['project' => $project]);
    }

    public function update(ProjectsRequest $request) {
        $project = Projects::where('id_user', '=', Auth::user()->id)->findOrFail($request->id);
        $project->name = trim($request->name);
        $project->description = trim($request->description);
        if (preg_replace('/\s+/', '', trim($request->url)) != '') {
            $project->link = trim($request->url);
        }
        if (trim($request->image_project) != null) {
            $extension = $request->image_project->clientExtension();
            $path = 'images/users/' . Auth::user()->id . '/projects';
            if (file_exists(public_path($project->image_url)) && $project->image_url != null) {
                unlink(public_path($project->image_url));
            }
            $project->image_url = $path . '/' . 'pr' . $project->id . '.' . $extension;
            $request->image_project->move(public_path($path), 'pr' . $project->id . '.' . $extension);
        }
        $project->save();
        return redirect('/admin/projects')->with('type', 'success-msg')
                        ->with('message', 'El proyecto se ha editado correctamente');
    }

    public function destroy(Request $request) {
        $project = Projects::where('id_user', '=', Auth::user()->id)->findOrFail($request->id_delete);
        $section = ProjectSections::where('id_project', '=', $request->id_delete);
        if (file_exists(public_path($project->image_url))) {
            if ($project->image_url != '') {
                unlink(public_path($project->image_url));
            }
        }
        if (is_dir('images/users/' . Auth::user()->id . '/projects/' . $project->id)) {
            foreach ($section->get() as $sectionImg) {
                if (file_exists(public_path($sectionImg->picture))) {
                    if ($sectionImg->picture != '') {
                        unlink(public_path($sectionImg->picture));
                    }
                }
            }
            rmdir('images/users/' . Auth::user()->id . '/projects/' . $project->id);
        }
        $section->delete();
        $project->delete();
        return redirect('/admin/projects')->with('type', 'success-msg')
                        ->with('message', 'El proyecto se ha eliminado con exito');
    }

}
