<?php

namespace App\Http\Controllers\Admin\Projects;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Admin\Projects\ProjectSections;
use App\Entities\Admin\Projects\Projects;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\Projects\ProjectSectionsRequest;

class ProjectSectionsController extends Controller {

    public function index($id) {
        $project = Projects::select('id', 'name', 'description', 'link', 'image_url')
                        ->where('id_user', '=', Auth::user()->id)->findOrFail($id);
        $sections = ProjectSections::select('id', 'title', 'description', 'picture', 'id_project')
                        ->where('id_project', '=', $project->id)->orderBy('created_at', 'asc')->get();
        return view('admin.projectSections.index', ['project' => $project, 'sections' => $sections]);
    }

    public function create($id) {
        $project = Projects::select('id', 'name')->where('id_user', '=', Auth::user()->id)->findOrFail($id);
        return view('admin.projectSections.create', ['project' => $project]);
    }

    public function store(ProjectSectionsRequest $request) {
        $project = Projects::select('id')->where('id_user', '=', Auth::user()->id)->findOrFail($request->id);

        $projectSection = new ProjectSections();
        $projectSection->title = trim($request->title_name);
        $projectSection->description = trim($request->description);
        $projectSection->id_project = $project->id;
        if (trim($request->image_section) != null) {
            $projectSection->save();
            $img = ProjectSections::findOrFail($projectSection->id);
            $extension = $request->image_section->clientExtension();
            $path = 'images/users/' . Auth::user()->id . '/projects/' . $project->id;
            if (file_exists(public_path($path . '/' . 'prs' . $img->id .'.'. $extension))) {
                unlink(public_path($path . '/' . 'prs' . $img->id .'.'. $extension));
            }
            $img->picture = $path . '/' . 'prs' . $img->id . '.' . $extension;
            $request->image_section->move(public_path($path), 'prs' . $img->id . '.' . $extension);
            $img->save();
        }
        $projectSection->save();
        return redirect('/admin/' . $project->id . '/project-sections')->with('type', 'success-msg')
                        ->with('message', 'La sección se ha gregado con exito');
    }

    public function edit($id) {
        $section = ProjectSections::select('project_sections.id as id', 'title', 'project_sections.description as description', \DB::raw("SUBSTRING_INDEX(picture,'/',-1) as picture"), 'projects.id as id_project')
                        ->where('id_user', '=', Auth::user()->id)
                        ->where('project_sections.id', '=', $id)
                        ->join('projects', 'projects.id', '=', 'id_project')->firstOrFail();
        return view('admin.projectSections.edit', ['section' => $section]);
    }

    public function update(ProjectSectionsRequest $request) {
        $projectSection = ProjectSections::findOrFail($request->id);
        $project = Projects::where('id_user', '=', Auth::user()->id)->findOrFail($projectSection->id_project);
        $projectSection->title = trim($request->title_name);
        $projectSection->description = trim($request->description);
        if (trim($request->image_section) != null) {
            $extension = $request->image_section->clientExtension();
            $path = 'images/users/' . Auth::user()->id . '/projects/' . $project->id;
            if (file_exists(public_path($projectSection->picture))) {
                if ($projectSection->picture != '') {
                    unlink(public_path($projectSection->picture));
                }
            }
            $projectSection->picture = $path . '/' . 'prs' . $projectSection->id . '.' . $extension;
            $request->image_section->move(public_path($path), 'prs' . $projectSection->id . '.' . $extension);
        }
        $projectSection->save();
        return redirect('/admin/' . $project->id . '/project-sections')->with('type', 'success-msg')
                        ->with('message', 'La sección se ha editado con exito');
    }

    public function destroy(Request $request) {
        $section = ProjectSections::findOrFail($request->id_delete);
        $project = Projects::where('id_user', '=', Auth::user()->id)->findOrFail($section->id_project);
        if (file_exists(public_path($section->picture))) {
            if ($section->picture != '') {
                unlink(public_path($section->picture));
            }
        }
        $section->delete();
        return redirect('/admin/' . $project->id . '/project-sections')->with('type', 'success-msg')
                        ->with('message', 'La sección se ha eliminado con exito');
    }

}
