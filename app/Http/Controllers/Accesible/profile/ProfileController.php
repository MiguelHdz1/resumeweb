<?php

namespace App\Http\Controllers\Accesible\profile;

use App\Entities\Admin\Experiences\Experiences;
use App\Http\Controllers\Controller;
use App\Entities\Admin\Users\Profiles;
use App\Entities\Admin\Users\Users;
use App\Entities\Admin\Networks\Networks;
use App\Entities\Admin\TypeSkills\TypeSkills;
use App\Entities\Admin\Skills\Skills;

class ProfileController extends Controller {

    public function index($user) {
        $user_data = Users::select('id')->where('user_link', '=', $user)->where('status', '!=', '0')->firstOrFail();        
        $id = $user_data->id;        
        $profile = Profiles::select(\DB::raw('DATE_FORMAT(birthday,"%d/%m/%Y") as birthday'),
                'phone_number', 'about_me', 'profession', 'current_job', 'lives_in', 'avatar',
                'background', 'name', 'first_name', 'last_name', 'email', 'user_link')
                        ->join('users', 'users.id', '=', 'profiles.id_user')->where('id_user','=',$id)->firstOrFail();
        $type_skills = TypeSkills::select('id', 'name', 'icon')->where('status', '!=', '0')->get();
        $skills = array();
        foreach ($type_skills as $type_skill) {
            $skill = Skills::select('name')->where('id_type_skill', '=', $type_skill->id)->where('id_user', '=', $id);
            if ($skill->count() > 0) {
                $skills[] = array('name' => $type_skill->name, 'icon' => $type_skill->icon, 'current_skills' => $skill->get());
            }
        }
        $experiences = Experiences::select('company', 'about', \DB::raw('DATE_FORMAT(initial_date,"%d/%m/%Y") as initial_date'), \DB::raw('DATE_FORMAT(final_date,"%d/%m/%Y") as final_date'), 'current')
                        ->where('id_user', '=', $id)->orderBy('current', 'desc')->orderBy('created_at', 'desc')->get();
        $networks = Networks::select(\DB::raw('CONCAT(SUBSTRING(url,1,28),"...") as url'), 'url as full_url', 'name', 'icon', 'status')->where('status', '!=', '0')
                        ->where('id_user', '=', $id)->join('network_types', 'network_types.id', '=', 'networks.id_type_network')->get();
        return view('public.profile.index', ['profile' => $profile, 'experiences' => $experiences, 'skills' => $skills, 'networks' => $networks]);
    }
    
    public function soRedirect($user){
        return redirect($user.'/profile');
    }

}
