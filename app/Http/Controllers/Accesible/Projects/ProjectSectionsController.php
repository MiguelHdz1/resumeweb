<?php

namespace App\Http\Controllers\Accesible\Projects;

use App\Http\Controllers\Controller;
use App\Entities\Admin\Projects\ProjectSections;
use App\Entities\Admin\Projects\Projects;
use App\Entities\Admin\Users\Users;
use App\Entities\Admin\Users\Profiles;

class ProjectSectionsController extends Controller {

    public function index($user,$id) {        
        $user_data = Users::select('id')->where('user_link', '=', $user)->where('status', '!=', '0')->firstOrFail();
        $id_user = $user_data->id;
        $profile = Profiles::select('avatar','background', 'name', 'first_name',
                'last_name', 'email', 'user_link')
                ->join('users', 'users.id', '=', 'profiles.id_user')->where('id_user','=',$id_user)->firstOrFail();
        $project = Projects::select('id', 'name', 'description', 'link', 'image_url')
                        ->where('id_user', '=', $id_user)->findOrFail($id);
        $sections = ProjectSections::select('id', 'title', 'description', 'picture', 'id_project')
                        ->where('id_project', '=', $project->id)->orderBy('created_at', 'asc')->get();
        return view('public.projectSections.index', ['project' => $project, 'sections' => $sections,'profile'=>$profile]);
    }   
}
