<?php

namespace App\Http\Controllers\Accesible\Projects;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Admin\Projects\Projects;
use App\Entities\Admin\Users\Users;
use App\Entities\Admin\Users\Profiles;

class ProjectsController extends Controller {

    public function index(Request $request,$user) {
        $button = false;
        $search = $request->input('search');
        $user_data = Users::select('id')->where('user_link', '=', $user)->where('status', '!=', '0')->firstOrFail();
        $id = $user_data->id;
        $profile = Profiles::select('avatar','background', 'name', 'first_name',
                'last_name', 'email', 'user_link')
                ->join('users', 'users.id', '=', 'profiles.id_user')->where('id_user','=',$id)->firstOrFail();
        $projects = Projects::select('name', 'description', 'id', 'image_url')->where('id_user', '=', $id);
        if ($search != '') {
            $button = true;
            $projects->where('name', 'like', '%' . $search . '%');
        }
        return view('public.projects.index', ['projects' => $projects->orderBy('created_at', 'desc')
                    ->paginate(6)->appends(['search' => $search]),
            'button' => $button, 'search' => $search,'profile'=>$profile]);
    } 
}
