<?php

namespace App\Http\Controllers\Accesible\Register;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Admin\Users\Users;
use App\Entities\Admin\Users\Profiles;
use App\Mail\Activation\ActivationMail;
use App\Entities\Admin\ActivationTokens\ActivationTokens;
use App\Http\Requests\Admin\Register\RegisterRequest;

class RegisterController extends Controller {

    public function index() {
        return view('public.register.index');
    }

    public function register(RegisterRequest $request) {
        $user = new Users();
        $profile = new Profiles();
        $user->email = trim($request->email);
        $user->password = bcrypt(trim($request->password));
        $user->type_user = '1';
        $user->status = '0';
        $user->user_link = trim($this->generateUserLink($request->name, $request->first_name, $request->last_name, 0));
        $user->save();
        $profile->name = trim($request->name);
        $profile->first_name = trim($request->first_name);
        $profile->last_name = trim($request->last_name);
        $profile->id_user = $user->id;
        $profile->save();
        $tokenGenerated = $this->generateToken($user->id);
        $token = new ActivationTokens();
        $token->token = $tokenGenerated;
        $token->id_user = $user->id;
        $token->save();
        try {
            \Mail::to($user->email)->send(new ActivationMail($request->name . ' ' . $request->first_name, $token->token));
        } catch (\Exception $ex) {
            
        }
        return redirect('/admin')->with('message', 'Te hemos enviado un correo de activación, ingresa a tu correo electrónico para poder activar tu cuenta.')
                        ->with('type', 'msg-modal')->with('title','Registro correcto');
    }

    public function activateAccount($token) {
        $findToken = ActivationTokens::where('token', '=', $token)->firstOrFail();
        $user = Users::where('status','=','0')->findOrFail($findToken->id_user);
        $user->status='1';
        $user->save();
        $findToken->delete();
        return redirect('/admin')->with('message', 'Felicidades, has completado tu registro, ahora puedes iniciar sesión y disfrutar.')
                        ->with('type', 'msg-modal')->with('title','¡Listo!');
    }

    public function generateUserLink($name, $firstName, $lastName, $id) {
        $userLink = strtolower('@' .
                        substr($this->deleteSpecialCharacters($name), 0, 2) .
                        substr($this->deleteSpecialCharacters($firstName), 0, 1) .
                        substr($this->deleteSpecialCharacters($lastName), 0, 1)) . str_random(1) . $id;
        $user = Users::where('user_link', '=', $userLink);
        if ($user->count() > 0) {
            $id++;
            return $this->generateUserLink($name, $firstName, $lastName, $id);
        } else {
            return $userLink;
        }
    }

    public function deleteSpecialCharacters($str) {
        $unwanted = array('Á', 'É', 'Í', 'Ó', 'Ú', 'á', 'é', 'í', 'ó', 'ú', 'ñ', 'Ñ');
        $replaceTo = array('A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u', 'n', 'N');
        return str_replace($unwanted, $replaceTo, $str);
    }

    public function generateToken($id) {
        $token = bin2hex(random_bytes(10)) . bin2hex($id) . bin2hex(str_random(1));
        $verify = ActivationTokens::where('token', '=', $token);
        if ($verify->count() > 0) {
            return $this->generateToken($id);
        } else {
            return $token;
        }
    }

}
