<?php

namespace App\Http\Controllers\Accesible\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Admin\Blog\Blogs;
use App\Entities\Admin\Users\Users;
use App\Entities\Admin\Users\Profiles;


class BlogController extends Controller {
    public function index(Request $request,$user) {
        $button = false;
        $search = $request->input('search');
        $user_data = Users::select('id')->where('user_link', '=', $user)->where('status', '!=', '0')->firstOrFail();
        $id = $user_data->id;
        $profile = Profiles::select(\DB::raw('DATE_FORMAT(birthday,"%d/%m/%Y") as birthday'),
                'phone_number', 'about_me', 'profession', 'current_job', 'lives_in', 'avatar',
                'background', 'name', 'first_name', 'last_name', 'email', 'user_link')
                        ->join('users', 'users.id', '=', 'profiles.id_user')
                ->where('id_user','=',$id)->firstOrFail();
        
        $blogs = Blogs::select('id', 'title', 'url_cover', 'description', \DB::raw('DATE_FORMAT(created_at,"%d/%m/%Y") as date'))
                ->where('id_user', '=', $id);
        if ($search != '') {
            $button = true;
            $blogs->where('title', 'like', '%' . $search . '%');
        }
        return view('public.blogs.index', ['profile'=>$profile,'blogs' => $blogs->orderBy('created_at', 'desc')->paginate(6)->appends(['search' => $search]), 'button' => $button, 'search' => $search]);
    }
    
    public function view($user,$id) {      
        $user_data = Users::select('id')->where('user_link', '=', $user)->where('status', '!=', '0')->firstOrFail();
        $id_user = $user_data->id;
        $profile = Profiles::select(\DB::raw('DATE_FORMAT(birthday,"%d/%m/%Y") as birthday'),
                'phone_number', 'about_me', 'profession', 'current_job', 'lives_in', 'avatar',
                'background', 'name', 'first_name', 'last_name', 'email', 'user_link')
                        ->join('users', 'users.id', '=', 'profiles.id_user')->where('id_user','=',$id_user)->firstOrFail();
        $blog = Blogs::select('title', 'content', \DB::raw('DATE_FORMAT(created_at,"%d/%m/%Y") as date'), 'description', 'url_cover')
                        ->where('id_user', '=', $id_user)->findOrFail($id);
        return view('public.blogs.preview', ['blog' => $blog,'profile'=>$profile]);
    }
}
