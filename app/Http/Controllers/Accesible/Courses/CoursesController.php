<?php

namespace App\Http\Controllers\Accesible\Courses;

use App\Entities\Admin\Courses\Courses;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Admin\Users\Users;
use App\Entities\Admin\Users\Profiles;

class CoursesController extends Controller {

    public function index(Request $request,$user) {
        $colors = ['light-blue darken-3', 'cyan darken-3', 'teal darken-3',
            'blue darken-3', 'indigo darken-2', 'red darken-3', 'yellow darken-3',
            'amber darken-3', 'orange darken-3', 'green darken-3', 'light-green darken-3',
            'amber accent-4', 'blue-grey darken-3', 'green darken-1'];
        $button = false;
        $search = $request->input('search');
        $user_data = Users::select('id')->where('user_link', '=', $user)->where('status', '!=', '0')->firstOrFail();
        $id = $user_data->id;
        $profile = Profiles::select(\DB::raw('DATE_FORMAT(birthday,"%d/%m/%Y") as birthday'),
                'phone_number', 'about_me', 'profession', 'current_job', 'lives_in', 'avatar',
                'background', 'name', 'first_name', 'last_name', 'email', 'user_link')
                        ->join('users', 'users.id', '=', 'profiles.id_user')
                ->where('id_user','=',$id)->firstOrFail();
        $courses = Courses::select('id', 'name', 'institute_name', 'file_type', 'file_url', 'license')
                ->where('id_user', '=', $id);
        if ($search) {
            $button = true;
            $courses->where('name', 'like', '%' . $search . '%');
        }

        return view('public.courses.index', ['courses' => $courses->orderBy('created_at', 'desc')
                ->paginate(6)->appends(['search' => $search])
            ,'profile'=>$profile, 'colors' => $colors, 'button' => $button, 'search' => $search]);
    }

    
    public function show($user,$id) {
        $user_data = Users::select('id')->where('user_link', '=', $user)->where('status', '!=', '0')->firstOrFail();
        $id_user = $user_data->id;
        $profile = Profiles::select(\DB::raw('DATE_FORMAT(birthday,"%d/%m/%Y") as birthday'),
                'phone_number', 'about_me', 'profession', 'current_job', 'lives_in', 'avatar',
                'background', 'name', 'first_name', 'last_name', 'email', 'user_link')
                        ->join('users', 'users.id', '=', 'profiles.id_user')
                ->where('id_user','=',$id_user)->firstOrFail();
        $course = Courses::select('name', 'institute_name', 'license',
                'file_type', 'description','file_url')
                        ->where('id_user', '=', $id_user)->findOrFail($id);
        return view('public.courses.show',['course'=>$course,'profile'=>$profile]);
    }
}
