<?php

namespace App\Http\Requests\Admin\Courses;

use Illuminate\Foundation\Http\FormRequest;

class CoursesRequest extends FormRequest
{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'name'=> 'required|max:100|min:5',
            'institution'=> 'required|min:3|max:150',
            'license'=> 'max:255|min:10|nullable',                        
            'type_course'=> 'required|max:1|min:1|in:0,1,2',                        
            'description'=> 'required|max:700|min:10',                        
            'file_course'=> 'max:2024|mimes:pdf|required',
                  
        ];
    }
    
    public function messages() {
        return [
            'name.required'=>'El campo es obligatorio',
            'name.max'=>'El campo debe ser menor o igual a 100 caracteres',
            'name.min'=>'El campo debe ser mayor o igual a 5 caracteres',
            'institution.required'=>'El campo es obligatorio',
            'institution.max'=>'El campo debe ser menor o igual a 150 caracteres',
            'institution.min'=>'El campo debe ser mayor o igual a 3 caracteres',            
            'license.max'=>'El campo debe ser menor o igual a 255 caracteres',
            'license.min'=>'El campo debe ser mayor o igual a 10 caracteres',
            'type_course.required'=>'El campo es obligatorio',
            'type_course.max'=>'El campo debe ser menor o igual a 1 caracteres',
            'type_course.min'=>'El campo debe ser mayor o igual a 1 caracteres',
            'type_course.in'=>'El valor no es valido',
            'description.required'=>'El campo es obligatorio',
            'description.max'=>'El campo debe ser menor o igual a 700 caracteres',
            'description.min'=>'El campo debe ser mayor o igual a 10 caracteres',
            'file_course.max'=>'El archivo no debe ser mayor a 5 megabytes',
            'file_course.mimes'=>'El archivo debe ser formato pdf',            
            'file_course.required'=>'El campo es obligatorio'            
        ];
    }
}
