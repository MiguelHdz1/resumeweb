<?php

namespace App\Http\Requests\Admin\Projects;

use Illuminate\Foundation\Http\FormRequest;
class ProjectsRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules(){
        return [
            'image_project'=> 'max:5024|mimes:png,jpg,jpeg,gif',            
            'name'=> 'required|max:40|min:3',
            'url'=> 'max:100|min:5|url|nullable',
            'description'=> 'required|min:10|max:300'            
        ];
    }
    
    public function messages() {
        return [
            'image_project.max'=>'La imagen no debe ser mayor a 5 megabytes',
            'image_project.mimes'=>'El archivo debe ser una imagen(png,jpg.jpeg,gif)',            
            'name.required'=>'El campo es obligatorio',
            'name.max'=>'El campo debe ser menor o igual a 40 caracteres',
            'name.min'=>'El campo debe ser mayor o igual a 3 caracteres',            
            'description.required'=>'El campo es obligatorio',
            'description.max'=>'El campo debe ser menor o igual a 300 caracteres',
            'description.min'=>'El campo debe ser mayor o igual a 10 caracteres',            
            'url.url'=>'El campo debe ser una url valida',
            'url.max'=>'El campo debe ser menor o igual a 100 caracteres',
            'url.min'=>'El campo debe ser mayor o igual a 5 caracteres',            
        ];
    }
}
