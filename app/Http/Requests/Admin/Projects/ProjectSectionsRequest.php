<?php

namespace App\Http\Requests\Admin\Projects;

use Illuminate\Foundation\Http\FormRequest;
class ProjectSectionsRequest extends FormRequest
{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'image_section'=> 'max:5024|mimes:png,jpg,jpeg,gif',            
            'title_name'=> 'required|max:40|min:3',            
            'description'=> 'required|min:10|max:300'            
        ];
    }
    
    public function messages() {
        return [
            'image_section.max'=>'La imagen no debe ser mayor a 5 megabytes',
            'image_section.mimes'=>'El archivo debe ser una imagen(png,jpg.jpeg,gif)',            
            'title_name.required'=>'El campo es obligatorio',
            'title_name.max'=>'El campo debe ser menor o igual a 40 caracteres',
            'title_name.min'=>'El campo debe ser mayor o igual a 3 caracteres',            
            'description.required'=>'El campo es obligatorio',
            'description.max'=>'El campo debe ser menor o igual a 300 caracteres',
            'description.min'=>'El campo debe ser mayor o igual a 10 caracteres'
        ];
    }
}
