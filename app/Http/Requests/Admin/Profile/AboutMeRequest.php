<?php

namespace App\Http\Requests\Admin\Profile;

use Illuminate\Foundation\Http\FormRequest;

class AboutMeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'about_me'=> 'required|max:500|min:20'
        ];
    }
    
    public function messages() {
        return [
            'about_me.required'=>'El campo es obligatorio',
            'about_me.max'=>'El campo no debe ser mayor a 500 caracteres',
            'about_me.min'=>'El campo debe tener al menos 20 caracteres', 
        ];
    }
}
