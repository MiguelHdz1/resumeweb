<?php

namespace App\Http\Requests\Admin\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class PresentationRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules(){
        return [
            'file_profile'=> 'max:5024|mimes:png,jpg,jpeg,gif',
            'file_back'=> 'max:5024|mimes:png,jpg,jpeg,gif',
            'name'=> 'required|max:100|min:3|regex:/^(?:[A-Za-z áÁéÉíÍóÓúÚ]+)$/',
            'first_name'=> 'required|max:100|min:3|regex:/^(?:[A-Za-z áÁéÉíÍóÓúÚ]+)$/',
            'last_name'=> 'required|max:100|min:3|regex:/^(?:[A-Za-z áÁéÉíÍóÓúÚ]+)$/',
            'phone'=> 'numeric|digits:10|required',
            'password'=> 'max:20|min:7|confirmed|regex:/^\S*$/u|nullable',
        ];
    }
    
    public function messages() {
        return [
            'file_profile.max'=>'La imagen no debe ser mayor a 5 megabytes',
            'file_profile.mimes'=>'El archivo debe ser una imagen(png,jpg.jpeg,gif)',
            
            'file_back.max'=>'La imagen no debe ser mayor a 5 megabytes',
            'file_back.mimes'=>'El archivo debe ser una imagen(png,jpg.jpeg,gif)',          
            
            'name.required'=>'El campo es obligatorio',
            'name.max'=>'El campo debe ser menor o igual a 100 caracteres',
            'name.min'=>'El campo debe ser mayor o igual a 3 caracteres',
            'name.regex'=>'Formato no valido',
            
            'first_name.required'=>'El campo es obligatorio',
            'first_name.max'=>'El campo debe ser menor o igual a 100 caracteres',
            'first_name.min'=>'El campo debe ser mayor o igual a 3 caracteres',
            'first_name.regex'=>'Formato no valido',
            
            'last_name.required'=>'El campo es obligatorio',
            'last_name.max'=>'El campo debe ser menor o igual a 100 caracteres',
            'last_name.min'=>'El campo debe ser mayor o igual a 3 caracteres',
            'last_name.regex'=>'Formato no valido',
            
            'phone.numeric'=>'El campo debe ser númerico', 
            'phone.digits'=>'Ingresar teléfono a 10 digitos', 
            'phone.required'=>'El campo es obligatorio', 
            'password.min'=>'El campo debe ser mayor o igual a 7 caracteres',
            'password.max'=>'El campo debe ser menor o igual a 20 caracteres',
            'password.confirmed'=>'Confirmar contraseña',
            'password.regex'=>'Formato no valido, no se permiten espacios'
        ];
    }
}
