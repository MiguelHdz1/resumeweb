<?php

namespace App\Http\Requests\Admin\Profile;

use Illuminate\Foundation\Http\FormRequest;

class PersonalInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profession'=> 'required|max:80|min:5|regex:/^(?:[A-Za-z áÁéÉíÍóÓúÚ 0-9]+)$/',
            'current_job'=> 'required|max:150|min:5|regex:/^(?:[A-Za-z áÁéÉíÍóÓúÚ 0-9]+)$/',
            'birthday'=> 'required|date_format:d/m/Y|before:01/01/2001|after:31/11/1969',
            'lives_in'=> 'required|max:100|min:5|regex:/^(?:[A-Za-z áÁéÉíÍóÓúÚ 0-9]+)$/',
        ];
    }
    
    public function messages() {
        return [
            'profession.required'=>'El campo es obligatorio',
            'profession.max'=>'El campo debe ser menor o igual a 80 caracteres',
            'profession.min'=>'El campo debe ser mayor o igual a 5 caracteres',
            'profession.regex'=>'Formato no valido',
            'current_job.required'=>'El campo es obligatorio',
            'current_job.max'=>'El campo debe ser menor o igual a 150 caracteres',
            'current_job.min'=>'El campo debe ser mayor o igual a 5 caracteres',
            'current_job.regex'=>'Formato no valido',
            'birthday.before'=>'La fecha de nacimiento debe ser menor a 01/01/2001.', 
            'birthday.after'=>'La fecha de cumpleaños debe ser mayor a 31/11/1969', 
            'birthday.date'=>'La fecha no es valida', 
            'birthday.date_format'=>'Formato de fecha no valido', 
            'birthday.required'=>'El campo es obligatorio', 
            'lives_in.required'=>'El campo es obligatorio',
            'lives_in.max'=>'El campo debe ser menor o igual a 100 caracteres',
            'lives_in.min'=>'El campo debe ser mayor o igual a 5 caracteres',
            'lives_in.regex'=>'Formato no valido'
        ];
    }
}
