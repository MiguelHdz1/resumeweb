<?php

namespace App\Http\Requests\Admin\Skills;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SkillsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> ['required','max:60','min:3', Rule::unique('skills')->ignore($this->id,'id')->where(function($query){
                return $query->where('id_user','=',\Auth::user()->id);
            })],
            'type_skill'=> ['required', Rule::exists('type_skills','id')->where(function($query){
                return $query->where('status','!=','0');
            })]
        ];
    }
    
    public function messages() {
        return [
            'name.required'=>'El campo es obligatorio',
            'name.max'=>'El campo no debe ser mayor a 60 caracteres',
            'name.unique'=>'La habilidad "'.$this->name.'" ya existe',
            'name.min'=>'El campo debe tener al menos 3 caracteres',            
            'type_skill.exists'=>'El campo no es un valor valido',            
            'type_skill.required'=>'El campo es obligatorio'            
        ];
    }
}
