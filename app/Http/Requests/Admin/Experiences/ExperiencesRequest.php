<?php

namespace App\Http\Requests\Admin\Experiences;

use Illuminate\Foundation\Http\FormRequest;

class ExperiencesRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules(){
        return [
            'company'=> 'required|max:50|min:3',
            'initial_date'=> 'required|date_format:d/m/Y|before_or_equal:'.date('d/m/Y').'|after:31/11/1980',
            'final_date'=> 'required|date_format:d/m/Y|before_or_equal:'.date('d/m/Y').'|after_or_equal:initial_date',            
            'about'=> 'required|max:500|min:5',
        ];
    }
    
    public function messages() {
        return [
            'company.required'=>'El campo es obligatorio',
            'company.max'=>'El campo debe ser menor o igual a 50 caracteres',
            'company.min'=>'El campo debe ser mayor o igual a 3 caracteres',
            'initial_date.date_format'=>'Formato de fecha no valido', 
            'initial_date.before_or_equal'=>'La fecha debe ser menor o igual a '.date('d/m/Y'), 
            'initial_date.after'=>'La fecha debe ser mayor a 31/11/1980', 
            'final_date.after_or_equal'=>'La fecha debe ser mayor o igual a la fecha de inicio', 
            'final_date.before_or_equal'=>'La fecha debe ser menor o igual a '.date('d/m/Y'),
            'about.required'=>'El campo es obligatorio',
            'about.max'=>'El campo debe ser menor o igual a 500 caracteres',
            'about.min'=>'El campo debe ser mayor o igual a 5 caracteres'
        ];
    }
}