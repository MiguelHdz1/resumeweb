<?php

namespace App\Http\Requests\Admin\Blog;

use Illuminate\Foundation\Http\FormRequest;

class BlogCategorieRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'name'=> 'required|max:60|min:5'
            ];
    }
    
    public function messages() {
        return [
            'name.required'=>'El campo es obligatorio',
            'name.max'=>'El campo debe ser menor o igual a 60 caracteres',
            'name.min'=>'El campo debe ser mayor o igual a 5 caracteres',                        
        ];
    }
}