<?php

namespace App\Http\Requests\Admin\Blog;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class BlogsRequest extends FormRequest
{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'title'=> 'required|max:60|min:5',
            'categorie'=> ['required',Rule::exists('blog_categories','id')->where(function($query){
                $query->where('status','!=','0');
            })],
            'description'=> 'required|min:10|max:150',
            'picture'=> 'max:5024|mimes:png,jpg,jpeg,gif',                        
            'blog_area'=> 'required',                        
                  
        ];
    }
    
    public function messages() {
        return [
            'picture.max'=>'La imagen no debe ser mayor a 5 megabytes',
            'picture.mimes'=>'El archivo debe ser una imagen(png,jpg,jpeg,gif)',
            'title.required'=>'El campo es obligatorio',
            'title.max'=>'El campo debe ser menor o igual a 60 caracteres',
            'title.min'=>'El campo debe ser mayor o igual a 5 caracteres',            
            'description.required'=>'El campo es obligatorio',
            'description.max'=>'El campo debe ser menor o igual a 150 caracteres',
            'description.min'=>'El campo debe ser mayor o igual a 10 caracteres',
            'categorie.required'=>'El campo es obligatorio',
            'categorie.exists'=>'Valor no valido',
            'blog_area.required'=>'El campo es obligatorio',
            'blog_area.max'=>'El blog supera el numero de caracteres permitidos',
        ];
    }
}
