<?php

namespace App\Http\Requests\Admin\TypeSkills;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TypeSkillsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> ['required','max:80','min:3', Rule::unique('type_skills')->ignore($this->id,'id')->where(function($query){
                return $query->where('status','!=','0');
            })],
            'icon'=> 'required|max:50|min:3',
        ];
    }
    
    public function messages() {
        return [
            'name.required'=>'El campo es obligatorio',
            'name.max'=>'El campo no debe ser mayor a 80 caracteres',
            'name.unique'=>'El valor '.$this->name.' ya existe',
            'name.min'=>'El campo debe tener al menos 3 caracteres',
            'icon.min'=>'El campo debe tener al menos 3 caracteres',
            'icon.required'=>'El campo es obligatorio',
            'icon.max'=>'El campo no debe ser mayor a 50 caracteres',
        ];
    }
}
