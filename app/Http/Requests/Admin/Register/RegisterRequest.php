<?php

namespace App\Http\Requests\Admin\Register;

use Illuminate\Foundation\Http\FormRequest;
class RegisterRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules(){
        return [
            'name'=> 'required|max:100|min:3|regex:/^(?:[A-Za-z áÁéÉíÍóÓúÚ]+)$/',
            'first_name'=> 'required|max:100|min:3|regex:/^(?:[A-Za-z áÁéÉíÍóÓúÚ]+)$/',
            'last_name'=> 'required|max:100|min:3|regex:/^(?:[A-Za-z áÁéÉíÍóÓúÚ]+)$/',
            'email'=> 'required|max:150|email|unique:users',
            'password'=> 'max:20|min:7|confirmed|regex:/^\S*$/u',        
        ];
    }
    
    public function messages() {
        return [
            'name.required'=>'El campo es obligatorio',
            'name.max'=>'El campo debe ser menor o igual a 100 caracteres',
            'name.min'=>'El campo debe ser mayor o igual a 3 caracteres',
            'name.regex'=>'Formato no valido',
            
            'first_name.required'=>'El campo es obligatorio',
            'first_name.max'=>'El campo debe ser menor o igual a 100 caracteres',
            'first_name.min'=>'El campo debe ser mayor o igual a 3 caracteres',
            'first_name.regex'=>'Formato no valido',
            
            'last_name.required'=>'El campo es obligatorio',
            'last_name.max'=>'El campo debe ser menor o igual a 100 caracteres',
            'last_name.min'=>'El campo debe ser mayor o igual a 3 caracteres',
            'last_name.regex'=>'Formato no valido',
            'email.max'=>'El campo debe ser menor o igual a 150 caracteres', 
            'email.email'=>'El email no es valido', 
            'email.required'=>'El campo es obligatorio',
            'email.unique'=>'El email ya se encuentra registrado',
            
            'password.min'=>'El campo debe ser mayor o igual a 7 caracteres',
            'password.max'=>'El campo debe ser menor o igual a 20 caracteres',
            'password.confirmed'=>'Confirmar contraseña',
            'password.regex'=>'Formato no valido, no se permiten espacios'
        ];
    }
}
