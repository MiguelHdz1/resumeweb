<?php

namespace App\Http\Requests\Admin\Networks;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NetworksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url'=> ['required','max:200','min:10', Rule::unique('networks')->ignore($this->id,'id')->where(function($query){
                return $query->where('id_user','=',\Auth::user()->id);
            })],
            'id_type'=> ['required', Rule::exists('network_types','id')->where(function($query){
                return $query->where('status','!=','0');
            })]
        ];
    }
    
    public function messages() {
        return [
            'url.required'=>'El campo es obligatorio',
            'url.max'=>'El campo no debe ser mayor a 200 caracteres',
            'url.unique'=>'La red ya existe',
            'url.min'=>'El campo debe tener al menos 10 caracteres',            
            'id_type.exists'=>'El campo no es un valor valido',            
            'id_type.required'=>'El campo es obligatorio'            
        ];
    }
}
