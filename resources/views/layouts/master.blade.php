<html>
    <head>        
        <meta name="theme-color" content="#244759" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{asset('materialize/css/materialize.min.css')}}" rel="stylesheet">
        <link rel="icon" href="{{url('images/kisspng.png')}}">
        <link href="{{asset('css/master.css')}}" rel="stylesheet">   
        @yield('css')
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>@yield('title')</title>
    </head>
    @if(Session::has('message'))
    <div class="message" id="{{Session('type')}}" data-bind="{{Session('message')}}">            
    </div>
    @endif
    <body>        
        <header>
            <div class="navbar-fixed z-depth-2">
                <nav class="blue-dark-meteor content">
                    <div class="nav-wrapper">
                        <h5 href="#" class="brand-logo center hide-on-small-and-down">@yield('page_name')</h5>
                        <label href="#" class="white-text title-txt center hide-on-med-and-up">@yield('page_name')</label>
                        <ul id="dropdown1" class="dropdown-content">
                            <li><a target="_blank" href="{{route('public.so_redirect',['user'=>Auth::user()->user_link])}}">As&iacute; me veo</a></li>
                            <li><a href="#!">Acerca de</a></li>
                            <li class="divider"></li>
                            <li><a class="blue-grey-text" href="{{route('logout')}}"><i class="material-icons">exit_to_app</i>Salir</a></li>
                        </ul>
                        <ul class="right hide-on-med-and-down">
                            <li>
                                <a class="dropdown-trigger" href="#!" data-target="dropdown1">Opciones
                                    <i class="material-icons right">apps</i>
                                </a>
                            </li>
                        </ul>
                        <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                    </div>
                </nav>
            </div>
            <ul id="slide-out" class="sidenav sidenav-fixed z-depth-2">
                <li>
                    <div class="user-view">
                        <div class="background light-blue darken-3">
                            <img class="new-responsive"  src="{{asset(Auth::user()->background)}}">
                        </div>
                        <a href="#user"><img class="circle blue-dark-meteor" src="{{asset(Auth::user()->avatar)}}"></a>
                        <a href="#name"><span class="white-text name">{{Auth::user()->name.
                                    ' '.Auth::user()->first_name.' '.Auth::user()->last_name}}</span></a>
                        <a href="#email"><span class="white-text email">{{Auth::user()->email}}</span></a>
                    </div>
                </li>                                           
                <li><a class="waves-effect waves-meteor" href="/admin/profile"><i class="material-icons">account_circle</i>Perfil</a></li>                
                <li><a class="waves-effect waves-meteor" href="/admin/projects"><i class="material-icons">developer_mode</i>Proyectos</a></li>                
                <li><a class="waves-effect waves-meteor" href="{{route('admin.blog')}}"><i class="material-icons">library_books</i>Blog / Writeup</a></li>                                
                <li><a class="waves-effect waves-meteor" href="/admin/courses"><i class="material-icons">folder_special</i>Cursos/Certificados</a></li>                   
                <li><a class="waves-effect waves-meteor" href="/admin/tutorials"><i class="material-icons">book</i>Tutoriales</a></li>
                <li><div class="divider"></div></li>
                @if(Auth::user()->type_user == '2')
                <li><a class="waves-effect waves-meteor" href="/admin/network-types"><i class="material-icons">share</i>Redes</a></li>                
                <li><a class="waves-effect waves-meteor" href="/admin/type-skills"><i class="material-icons">palette</i>Habilidades</a></li>                
                <li><a class="waves-effect waves-meteor" href="/admin/blog-categories"><i class="material-icons">rate_review</i>Categorias (Blog)</a></li>                
                <li class="hide-on-large-only"><div class="divider"></div></li>
                @endif
                <li class="hide-on-large-only">                
                    <a class="waves-effect waves-meteor" href="{{route('public.so_redirect',['user'=>Auth::user()->user_link])}}"><i class="material-icons">mood</i>As&iacute; me veo</a>
                </li>
                <li class="hide-on-large-only">
                    <a class="waves-effect waves-meteor" href="{{route('logout')}}"><i class="material-icons">exit_to_app</i>Cerrar sesi&oacute;n</a>
                </li>
            </ul>
        </header>
        <main class="content container">
            @yield('content')            
        </main>
        <footer class="blue-dark-meteor content">
            <div class="footer-copyright">
                <div class="container white-text">
                    <div class="center-align">
                        @php
                        $year = date('Y');
                        @endphp
                        {{$year}}
                        <a class="grey-text text-lighten-4 right quote-rand">                            
                        </a>
                    </div>
                </div>
            </div>
        </footer>  
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/master.js')}}"></script>
        <script type="text/javascript" src="{{asset('materialize/js/materialize.min.js')}}"></script>   
        @yield('javascript')
    </body>
</html>