<html>
    <head>        
        <meta name="theme-color" content="#244759" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{asset('materialize/css/materialize.min.css')}}" rel="stylesheet">
        <link rel="icon" href="{{url('images/kisspng.png')}}">
        <link href="{{asset('css/master.css')}}" rel="stylesheet">   
        @yield('css')
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>@yield('title')</title>
    </head>
    @if(Session::has('message'))
    <div class="message" id="{{Session('type')}}" data-bind="{{Session('message')}}">            
    </div>
    @endif
    <body>        
        <header>
            <div class="navbar-fixed z-depth-2">
                <nav class="blue-dark-meteor content">
                    <div class="nav-wrapper">
                        <h5 href="#" class="brand-logo center hide-on-small-and-down">@yield('page_name')</h5>
                        <label href="#" class="white-text title-txt center hide-on-med-and-up">@yield('page_name')</label>
                        <ul id="dropdown1" class="dropdown-content">                            
                            @if(Auth::check())
                            <li><a class="blue-grey-text" href="/">Mi perfil</a></li>
                            <li class="divider"></li>
                            <li><a class="blue-grey-text" href="{{route('logout')}}"><i class="material-icons">exit_to_app</i>Salir</a></li>                            
                            @else
                            <li><a class="blue-grey-text" href="#">Registrarme</a></li>
                            <li class="divider"></li>
                            <li><a class="blue-grey-text" href="/">Iniciar sesi&oacute;n</a></li>
                            @endif
                        </ul>
                        <ul class="right hide-on-med-and-down">
                            <li>
                                <a class="dropdown-trigger" href="#!" data-target="dropdown1">Opciones
                                    <i class="material-icons right">apps</i>
                                </a>
                            </li>
                        </ul>
                        <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                    </div>
                </nav>
            </div>
            <ul id="slide-out" class="sidenav sidenav-fixed z-depth-2">
                <li>
                    <div class="user-view">
                        <div class="background light-blue darken-3">
                            <img class="new-responsive"  src="@yield('background')">
                        </div>
                        <a href="#user"><img class="circle blue-dark-meteor" src="@yield('avatar')"></a>
                        <a href="#name"><span class="white-text name">@yield('name')</span></a>
                        <a href="#email"><span class="white-text email">@yield('email')</span></a>
                    </div>
                </li>                                           
                <li><a class="waves-effect waves-meteor" href="/@yield('routing')/profile"><i class="material-icons">account_circle</i>Perfil</a></li>
                <li><a class="waves-effect waves-meteor" href="/@yield('routing')/projects"><i class="material-icons">developer_mode</i>Proyectos</a></li>                
                <li><a class="waves-effect waves-meteor" href="/@yield('routing')/blog"><i class="material-icons">library_books</i>Blog / Writeup</a></li>                
                <li><a class="waves-effect waves-meteor" href="/@yield('routing')/courses"><i class="material-icons">folder_special</i>Cursos/Certificados</a></li>                
                <li><a class="waves-effect waves-meteor" href="/@yield('routing')/tutorials"><i class="material-icons">book</i>Tutoriales</a></li>
                <li><div class="divider"></div></li>                
                @if(Auth::check())
                <li class="hide-on-large-only">
                    <a class="waves-effect waves-meteor" href="/"><i class="material-icons">person_pin</i>Mi Perfil</a>
                </li>

                <li class="hide-on-large-only">
                    <a class="waves-effect waves-meteor" href="{{route('logout')}}"><i class="material-icons">exit_to_app</i>Cerrar sesi&oacute;n</a>
                </li>
                @else
                <li class="hide-on-large-only">
                    <a class="waves-effect waves-meteor" href="#!"><i class="material-icons">person_add</i>Registrarme</a>
                </li>

                <li class="hide-on-large-only">
                    <a class="waves-effect waves-meteor" href="/"><i class="material-icons">lock</i>Iniciar sesi&oacute;n</a>
                </li>
                @endif
            </ul>
        </header>
        <main class="content container">
            @yield('content')            
        </main>
        <footer class="blue-dark-meteor content">
            <div class="footer-copyright">
                <div class="container white-text">
                    <div class="center-align">
                        @php
                        $year = date('Y');
                        @endphp
                        {{$year}}
                        <a class="grey-text text-lighten-4 right quote-rand">                            
                        </a>
                    </div>
                </div>
            </div>
        </footer>  
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/master.js')}}"></script>
        <script type="text/javascript" src="{{asset('materialize/js/materialize.min.js')}}"></script>   
        @yield('javascript')
    </body>
</html>