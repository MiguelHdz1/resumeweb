<html>
    <head>        
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{asset('materialize/css/materialize.min.css')}}" rel="stylesheet">
        <link rel="icon" href="{{url('images/kisspng.png')}}">
        <link href="{{asset('css/development.css')}}" rel="stylesheet">   
        @yield('css')
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>@yield('title')</title>
    </head>
    <body>        
        <header>
        </header>
        <main class="content container">
            <div class="my-wrapper valign-wrapper center-align">
                <div class="row">
                    <div class="col s12 white-text">
                        <h1 class="title-error">Página</h1>
                        <h3>en</h3>
                        <h3>construcci&oacute;n</h3>
                        <br>
                        <a href="/{{$button}}/" class="btn btn-large waves-effect waves-light orange darken-4">
                            Ir al inicio
                        </a>
                    </div>
                </div>
            </div>
        </main>
        <footer class="blue-dark-meteor content">
            <div class="footer-copyright">
                <div class="container white-text">
                    <div class="center-align">
                        @php
                        $year = date('Y');
                        @endphp
                        {{$year}}
                        <a class="grey-text text-lighten-4 right quote-rand">                            
                        </a>
                    </div>
                </div>
            </div>
        </footer>  
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/master.js')}}"></script>
        <script type="text/javascript" src="{{asset('materialize/js/materialize.min.js')}}"></script>   
        @yield('javascript')
    </body>
</html>