@extends('layouts/master')
@section('title','MR - Blog')
@section('page_name','Blog')
@section('css')
<link href="{{asset('js/admin/trumbowyg/dist/ui/trumbowyg.min.css')}}" rel="stylesheet">   
<link href="{{asset('css/admin/blog/index.css')}}" rel="stylesheet">   
@endsection
@section('content')
<br>
<div class="row">
    <form method="POST" action="{{route('admin.blog.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="input-field col s12 l6 m6">
            <input id="title" name="title" type="text" value="{{old('title')}}">
            <label for="title">Titulo</label>
            @foreach($errors->get('title') as $error)
            <span class="label-error"> {{$error}}</span>
            @endforeach   
        </div>
        <div class="input-field col s12 l6 m6">
            <select id="categorie" name="categorie">
                <option value="" disabled selected>Elige una opci&oacute;n</option>
                @foreach($categories as $categorie)
                <option value="{{$categorie->id}}">{{$categorie->name}}</option>
                @endforeach            
            </select>
            <label>Categor&iacute;a</label>
            @foreach($errors->get('categorie') as $error)
            <span class="label-error"> {{$error}}</span>
            @endforeach   
        </div>        
        <div class="input-field col s12 l6 m6">
            <textarea id="description" name="description" class="materialize-textarea">{{old('description')}}</textarea>
            <label for="description">Descripci&oacute;n breve</label>
            @foreach($errors->get('description') as $error)
            <span class="label-error"> {{$error}}</span>
            @endforeach   
        </div>
        <div class="file-field input-field col s12 l6 m6">
            <div class="btn">
                <span>Imagen</span>
                <input id="picture" name="picture" type="file">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" placeholder="(Opcional)">
                @foreach($errors->get('picture') as $error)
                <span class="label-error"> {{$error}}</span>
                @endforeach   
            </div>
        </div>
        <div class="col l12 s12">
            <textarea id="blog_area" name="blog_area" placeholder="Escribe tu blog aqui..." autofocus>{{old('blog_area')}}</textarea>
            @foreach($errors->get('blog_area') as $error)
            <span class="label-error"> {{$error}}</span>
            @endforeach   
        </div>

        <div class=" col s12">
            <div class="right-align">
                <a href="{{route('admin.blog')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <div class="s12 l12 m12 center-align">
        <button id="btn-preview" class="btn btn-small waves-effect">Vista previa</button>
        <button id="btn-hide-preview" class="btn btn-small waves-effect">Ocultar vista previa</button>
    </div>
</div>
<div class="row">
    <div class="preview col s12"></div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/trumbowyg/dist/trumbowyg.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/admin/trumbowyg/dist/plugins/pasteimage/trumbowyg.pasteimage.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/admin/trumbowyg/dist/langs/es.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery-resizable.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/admin/trumbowyg/dist/plugins/resizimg/trumbowyg.resizimg.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/admin/blog/blog.js')}}"></script>
@endsection