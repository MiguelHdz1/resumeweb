@extends('layouts/master')
@section('title','MR - Blog')
@section('page_name','Blog')
@section('content')
@section('css')
<link href="{{asset('css/admin/blog/index.css')}}" type="text/css" rel="stylesheet">
@endsection
<div class="row">
    <div class="col l4 offset-l1 s8 offset-s2 m5 offset-m1">        

        <div class="">
            <form method="GET" action="{{route('admin.blog')}}">
                <div class="input-field">
                    @if($button)
                    <a class="prefix" href="/admin/blog">
                        <i class="material-icons ">clear</i>
                    </a>
                    @endif
                    <input id="search" name="search" type="text" value="{{$search}}">
                    <label for="search">Buscar</label>
                </div>
            </form>
        </div>
    </div>
    <br><br><br><br>
    @if(sizeof($blogs))
    @foreach($blogs as $blog)
    <div class="col s12 l4 m6 white-text">
        <div class="card small card-smallest light-blue darken-2 tinted-image" style="background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.65)), url(&quot;{{asset($blog->url_cover)}}&quot;);">
            <div class="card-content">
                <div class="card-title">{{$blog->title}}</div>
                <p>{{$blog->description}}</p>
            </div>
            <div class="card-action light-blue darken-3">
                <a class="white-text">{{$blog->date}}</a>                                                           
                <a href="{{route('admin.blog.preview',['id'=>$blog->id])}}" class="white-text right tooltipped" data-position="top" data-tooltip="Leer">
                    <i class="material-icons">bookmark_border</i>
                </a>                                
                <a href="{{route('admin.blog.edit',['id'=>$blog->id])}}" class="white-text right tooltipped" data-position="top" data-tooltip="Editar">
                    <i class="material-icons">edit</i>
                </a>                                
                <a id="btn-delete" data-bind="{{$blog->id}}" class="white-text right tooltipped" data-position="top" data-tooltip="Eliminar">
                    <i class="material-icons">delete</i>
                </a>
            </div>
        </div>
    </div>    
    @endforeach
    <div id="div-more" class="center-align col s12">
        {{$blogs->links('vendor.pagination.default')}}
    </div>
    @else
    <br>
    <div class="col s8 offset-s2 center-align">
        <h5>Aun no has escrito ning&uacute;n blog</h5>
    </div>
    @endif
    <div class="fixed-action-btn">
        <a href="/admin/blog/create" class="btn-floating btn-large red waves-effect waves-light pulse">
            <i class="large material-icons">add</i>
        </a>
    </div>
</div>
<form id="form_delete" method="POST" action="{{route('admin.blog.delete')}}">
    @method('DELETE')
    @csrf
    <input type="hidden" id="id_delete" name="id_delete">
</form>

<div id="modal-delete" class="modal bottom-sheet">
    <div class="modal-content">
        <div class="row">
            <div class="col s12 l8 offset-l2">
                <h4>¿Deseas eliminar?</h4>
                <p>El eliminar este registro ya no permitira su uso, sin embargo sera visible para el perfil de los usuarios</p>
            </div>
        </div>        
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancelar</a>
        <a id="btn-delete-accept" href="#!" class="modal-close waves-effect waves-teal btn-flat">Aceptar</a>      
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/blog/index.js')}}"></script>
@endsection