@extends('layouts/master')
@section('title','MR - Vista previa')
@section('page_name','Vista previa')
@section('css')
<link href="{{asset('js/admin/trumbowyg/dist/ui/trumbowyg.min.css')}}" rel="stylesheet">
<link href="{{asset('css/admin/projects/sections.css')}}" rel="stylesheet">   
@endsection
@section('content')
<div class="row">
    <div class="col s12">
        <div class="card tinted-image sizing-back" style="background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.65)), url(&quot;{{asset($blog->url_cover)}}&quot;);">
            <div class="card-content white-text">
                <span class="card-title">{{$blog->title}}</span>
                <span>{{$blog->description}}</span><br>
                <span>Published: {{$blog->date}}</span>
            </div>
        </div>
    </div>
    <div class="col s12">
        <div class="show">
            {!!$blog->content!!}
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/blog/index.js')}}"></script>
@endsection