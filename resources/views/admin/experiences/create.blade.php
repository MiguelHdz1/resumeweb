@extends('layouts/master')
@section('title','MR - Agregar experiencia')
@section('page_name','Agregar experiencia')
@section('content')
<br>
<div class="row">
    <div class="col l6 offset-l3 s12 m8 offset-m2">
        <div class="card">
            <div class="card-content">
                <form method="POST" action="{{route('admin.experiences.store')}}">
                    @csrf
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="company" name="company" type="text" value="{{old('company')}}">
                            <label for="company">Empresa</label>                        
                            @foreach($errors->get('company') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="input-field col s12">
                            <input type="text" class="datepicker" id="initial_date" name="initial_date" value="{{old('initial_date')}}">
                            <label for="initial_date">Fecha de inicio</label>
                            @foreach($errors->get('initial_date') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach    
                        </div>
                        <div class="input-field col s12">
                            <input type="text" class="datepicker" id="final_date" name="final_date" value="{{old('final_date')}}">
                            <label for="final_date">Fecha de fin</label>
                            @foreach($errors->get('final_date') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach    
                        </div>
                        @if(!$current)
                        <div class="col s12">
                            <p>
                                <label>
                                    <input id="current" name="current" type="checkbox" />
                                    <span>Es mi empleo actual</span>
                                </label>
                            </p>
                        </div>
                        @endif
                        <div class="input-field col s12">
                            <textarea id="about" name="about" class="materialize-textarea" data-length="500">{{old('about')}}</textarea>
                            <label for="about">Acerca de tu experiencia</label>
                            @foreach($errors->get('about') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach    
                        </div>                        
                    </div>
                    <div class="right-align">
                        <a href="{{route('admin.experiences')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/networks/networks.js')}}"></script>
@endsection