@extends('layouts/master')
@section('title','MR - Crear categoría(Blog)')
@section('page_name','Crear categoría (Blog)')
@section('content')
<br>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <form method="POST" action="{{route('admin.blog_categories.store')}}">
                        @csrf
                        <div class="input-field col s12 l6 m6">
                            <input id="name" name="name" type="text" value="{{old('name')}}">
                            <label for="name">Nombre</label>
                            @foreach($errors->get('name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach   
                        </div>
                        <div class=" col s12">
                            <div class="right-align">
                                <a href="{{route('admin.blog_categories')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                                <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>
                            </div>
                        </div>
                    </form>  
                </div>
            </div>
        </div>
    </div>  
</div>
@endsection