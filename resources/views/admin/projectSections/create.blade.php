@extends('layouts/master')
@section('title','MR - Agregar sección')
@section('page_name','Agregar sección')
@section('content')
<br>
<div class="row">
    <div class="col s12 l8 m6 offset-l2 offset-m3">
        <div class="card">
            <div class="card-content">
                <span class="card-title center-align">{{$project->name}}</span>
                <div class="row">
                    <form method="post" action="{{route('admin.project_sections.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="col l10 s12 m10 offset-l1 offset-m1 input-field">
                            <input type="text" name="title_name" id="title_name" value="{{old('title_name')}}">
                            <label>Nombre</label>
                            @foreach($errors->get('title_name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach     
                        </div>
                        <div class="file-field input-field col l10 s12 m10 offset-l1 offset-m1">
                            <div class="btn">
                                <span>Imagen</span>
                                <input type="file" name="image_section" id="image_section">
                            </div>
                            <div class="file-path-wrapper">
                                <input id="img-name" name="img-name" class="file-path validate" type="text"  placeholder="(Opcional)">
                            </div>
                            @foreach($errors->get('image_section') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach 
                        </div>
                        <div class="input-field col l10 s12 m10 offset-l1 offset-m1">
                            <textarea id="description" name="description" class="materialize-textarea" data-length="500">{{old('description')}}</textarea>
                            <label for="description">Descripci&oacute;n</label>
                            @foreach($errors->get('description') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach 
                        </div>
                        <input type="hidden" value="{{$project->id}}" name="id" id="id"/>
                        <div class="right-align">
                        <a href="{{route('admin.project_sections',['id'=>$project->id])}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection