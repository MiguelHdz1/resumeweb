@extends('layouts/master')
@section('title','MR - Secciones')
@section('page_name','Secciones')
@section('css')
<link href="{{asset('css/admin/projects/sections.css')}}" rel="stylesheet">   
@endsection
@section('content')
<div class="row">
    <div class="col s12">
        <div class="card tinted-image sizing-back ">
            <div class="card-content white-text">
                <span class="card-title">{{$project->name}}</span>
                <p>{{$project->description}}</p>
            </div>
            @if($project->link !='')
            <div class="card-action right-align">
                <a class="btn btn-small waves-effect waves-light blue darken-1" target="_blank" href="{{$project->link}}">Repositorio</a>                
            </div>
            @endif
            <div class="image_back" data-bind="{{asset($project->image_url)}}"></div>
        </div>
    </div>
    @if(sizeof($sections)>0)

    @foreach($sections as $section)
    <div class="col s12">
        <div class="block ">
            <div class="divider"></div>
            <br>
            <div class="row">                
                <div class="col l7 s12 m10 offset-m1">
                    <img class="responsive-img materialboxed img-sections center-block" src="{{asset($section->picture)}}">
                </div>
                <div class="col l5 s12 m10 offset-m1">
                    <h5>{{$section->title}}</h5>
                    <p align="justify">{{$section->description}}</p>
                </div>
                <div class="col s12 right-align">
                    <a href="#!" data-bind="{{$section->id}}" class="btn-delete btn btn-small waves-effect waves-light tooltipped red darken-1" data-position="top" data-tooltip="Eliminar">
                        <i class="material-icons">delete</i>
                    </a>
                    <a href="{{route('admin.project_sections.edit',['id'=>$section->id])}}" class="btn btn-small  light-blue darken-2 waves-effect waves-light tooltipped" data-position="top" data-tooltip="Editar">
                        <i class="material-icons">edit</i>
                    </a>
                </div>
            </div>
        </div>

    </div>
    @endforeach

    @else
    <div class="col s12 center-align">
        <h5>Este proyecto no contiene secciones a&uacute;n</h5>
    </div>
    @endif
</div>

<div class="fixed-action-btn">
    <a href="{{route('admin.project_sections.create',['id'=>$project->id])}}"class="btn-floating btn-large red waves-effect waves-light pulse">
        <i class="large material-icons">add</i>
    </a>
</div>
<form id="form_delete" method="POST" action="{{route('admin.project_sections.delete')}}">
    @method('DELETE')
    @csrf
    <input type="hidden" id="id_delete" name="id_delete">
</form>
<div id="modal-delete" class="modal bottom-sheet">
    <div class="modal-content">
        <div class="row">
            <div class="col s12 l8 offset-l2">
                <h4>¿Deseas eliminar?</h4>
                <p>El eliminar este registro ya no permitira su uso, sin embargo sera visible para el perfil de los usuarios</p>
            </div>
        </div>        
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancelar</a>
        <a id="btn-delete-accept" href="#!" class="modal-close waves-effect waves-teal btn-flat">Aceptar</a>      
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/projects/sections.js')}}"></script>
<script type="text/javascript" src="{{asset('js/admin/projects/projects.js')}}"></script>
@endsection