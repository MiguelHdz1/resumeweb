@extends('layouts/master')
@section('title','MR - Editar sección')
@section('page_name','Editar sección')
@section('content')
<br>
<div class="row">
    <div class="col s12 l8 m6 offset-l2 offset-m3">
        <div class="card">
            <div class="card-content">                
                <div class="row">
                    <form method="post" action="{{route('admin.project_sections.update')}}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="col l10 s12 m10 offset-l1 offset-m1 input-field">
                            <input type="text" name="title_name" id="title_name" value="{{$section->title}}">
                            <label>Nombre</label>
                            @foreach($errors->get('title_name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach     
                        </div>
                        <div class="file-field input-field col l10 s12 m10 offset-l1 offset-m1">
                            <div class="btn">
                                <span>Imagen</span>
                                <input type="file" name="image_section" id="image_section">
                            </div>
                            <div class="file-path-wrapper">
                                <input id="img-name" value="{{$section->picture}}" name="img-name" class="file-path validate" type="text"  placeholder="(Opcional)">
                            </div>
                            @foreach($errors->get('image_section') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach 
                        </div>
                        <div class="input-field col l10 s12 m10 offset-l1 offset-m1">
                            <textarea id="description" name="description" class="materialize-textarea" data-length="500">{{$section->description}}</textarea>
                            <label for="description">Descripci&oacute;n</label>
                            @foreach($errors->get('description') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach 
                        </div>
                        <input type="hidden" value="{{$section->id}}" name="id" id="id"/>
                        <input type="hidden" value="{{$section->id_project}}" name="id_project" id="id_project"/>
                        <div class="right-align">
                        <a href="{{route('admin.project_sections',['id'=>$section->id_project])}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection