@extends('layouts/master')
@section('title','MR - Agregar cursos/certificados')
@section('page_name','Agregar cursos')

@section('content')
<br>
<div class="row">
    <div class="col s12 l8 m10 offset-l2 offset-m1">
        <div class="card">
            <div class="card-content">
                <form method="POST" action="{{route('admin.courses.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="input-field col s12 l6 m6">
                            <input id="name" name="name" type="text" value="{{old('name')}}">
                            <label for="name">Nombre del curso o certificaci&oacute;n</label>
                            @foreach($errors->get('name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="input-field col s12 l6 m6">
                            <input id="institution" name="institution" type="text" value="{{old('institution')}}">
                            <label for="institution">Instituci&oacute;n que certifica</label>                        
                            @foreach($errors->get('institution') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="input-field col s12 l6 m6">
                            <input id="license" name="license" type="text" value="{{old('license')}}">
                            <label for="license">Licencia (opcional)</label>                        
                            @foreach($errors->get('license') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="input-field col s12 l6 m6">
                            <select id="type_course" name="type_course">
                                <option value="" disabled selected>Elige una opci&oacute;n</option>
                                <option value="0">Certificaci&oacute;n</option>
                                <option value="1">Curso</option>                                
                                <option value="2">Otro</option>                                
                            </select>
                            <label for="type_course">¿Que es?</label>                    
                            @foreach($errors->get('type_course') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="input-field col s12">
                            <textarea class="materialize-textarea" name="description" id="description">{{old('description')}}</textarea>                                                 
                            <label for="description">Descripci&oacute;n del curso</label>
                            @foreach($errors->get('description') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="file-field input-field col s12">
                            <div class="btn">
                                <span>PDF</span>
                                <input id="file_course" name="file_course" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Comprobante">
                            </div>
                            @foreach($errors->get('file_course') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach    
                        </div>
                    </div>
                    <div class="right-align">
                        <a href="{{route('admin.courses')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection