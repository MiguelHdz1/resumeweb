@extends('layouts/master')
@section('title','MR - Cursos/Certificados')
@section('page_name','Cursos/Certificados')
@section('css')
<link href="{{asset('css/admin/blog/index.css')}}" type="text/css" rel="stylesheet">
@endsection
@section('content')
<br>
<div class="row">
    <div class="col l4 m4 offset-m1 s6 offset-l1 offset-s1">

        <form method="GET" action="{{route('admin.courses')}}">
            <div class="input-field">
                @if($button)
                <a class="prefix" href="{{route('admin.courses')}}">
                    <i class="material-icons ">clear</i>
                </a>
                @endif
                <input id="search" name="search" type="text" value="{{$search}}">
                <label for="search">Buscar</label>
            </div>
        </form>
    </div>
    <br>
    <br>
    <br>
    <br>
    @if(sizeof($courses)!=0)
    @foreach($courses as $course)
    <div class="col m6 l6 s12">
        <div class="card {{$colors[array_rand($colors,1)]}} small card-smallest">
            <div class="card-content white-text">                
                <span class="card-title">{{$course->name}}</span>
                <p>Otorga: {{$course->institute_name}}</p>                
                @if($course->file_type == '0')
                <p>Tipo: Certificaci&oacute;n</p>
                @elseif($course->file_type =='1')
                <p>Tipo: Curso</p>
                @else
                <p>Tipo: Otro</p>
                @endif
                <p>Licencia: 
                    @if($course->license !='')
                    {{$course->license}}
                    @else
                    N/A
                    @endif
                </p>                
            </div>
            <div class="card-action">
                <a id="btn-delete" name="btn-delete" data-bind="{{$course->id}}" class="white-text waves-effect waves-light">
                    <i class="material-icons">delete</i>
            </a>
            <a href="{{route('admin.courses.edit',['id'=>$course->id])}}" class=" white-text waves-effect waves-light">
                        <i class="material-icons">edit</i>
                </a>
                <a href="{{route('admin.courses.show',['id'=>$course->id])}}" class=" white-text waves-effect waves-light right">
                    ver
                </a>
            </div>
        </div>
    </div>    
    @endforeach    
    <div class="col s12 center-align">
        {{$courses->links('vendor.pagination.default')}}
    </div>
    @else
    <div class="center-align">
        <h5>No hay resultados</h5>
    </div>        
    @endif
    <div class="fixed-action-btn">
        <a href="{{route('admin.courses.create')}}" class="btn-floating btn-large red waves-effect waves-light pulse">
            <i class="large material-icons">add</i>
        </a>
    </div>
</div>
<form id="form_delete" method="POST" action="{{route('admin.courses.delete')}}">
    @method('DELETE')
    @csrf
    <input type="hidden" id="id_delete" name="id_delete">
</form>
<div id="modal-delete" class="modal bottom-sheet">
    <div class="modal-content">
        <div class="row">
            <div class="col s12 l8 offset-l2">
                <h4>¿Deseas eliminar?</h4>
                <p>El eliminar este registro ya no permitira su uso, sin embargo sera visible para el perfil de los usuarios</p>
            </div>
        </div>        
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancelar</a>
        <a id="btn-delete-accept" href="#!" class="modal-close waves-effect waves-teal btn-flat">Aceptar</a>      
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/networks/networks.js')}}"></script>
@endsection