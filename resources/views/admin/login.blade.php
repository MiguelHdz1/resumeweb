<!DOCTYPE html>
<html>
    <head>        
        <title>MR - Iniciar sesi&oacute;n</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="icon" href="{{url('images/kisspng.png')}}">
        <link href="{{asset('css/admin/login/login.css')}}" rel="stylesheet">
        <link href="{{asset('css/master.css')}}" rel="stylesheet">        
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{asset('materialize/css/materialize.min.css')}}" rel="stylesheet">
    </head>
    @if(Session::has('message'))
    <div class="message" id="{{Session('type')}}" data-bind="{{Session('message')}}">            
    </div>
    @endif
    <body>
        <nav>
            <div class="nav-wrapper blue-meteor">
                <a href="/" class="brand-logo">Logo</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a class="btn waves-effect waves-light orange darken-4" href="{{route('register')}}">
                            <i class="material-icons left">add</i>
                            Registrarme
                        </a>
                    </li>                  
                </ul>
            </div>
        </nav>        
        <main class="container center-align valign-wrapper">
            <div class="row">
                <div class="col s12 m8 l8 offset-l2 offset-m2">
                    <form method="POST" action="{{route('login')}}">
                        @csrf
                        <div class="card">                            
                            <div class="card-content">   
                                <div class="card-header space">
                                    <img class="responsive-img logo-img" src="{{asset('images/kisspng.png')}}">
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col s12 l10 offset-l1">
                                        <div class="input-field">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="email" name="email" type="email" class="validate">
                                            <label class="active" for="email">Correo electronico</label>
                                            <div class="red-text">
                                                @foreach($errors->get('email') as $error)
                                                {{$error}}
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s12 l10 offset-l1">
                                        <div class="input-field">
                                            <i class="material-icons prefix">lock</i>
                                            <input id="password" name="password" type="password" class="validate">
                                            <label class="active" for="password">Contrase&ntilde;a</label>
                                            <div class="red-text">
                                                @foreach($errors->get('mail') as $error)
                                                {{$error}}
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s12">
                                        <br>
                                        <button type="submit" class="btn btn-large orange darken-4 waves-effect waves-green">Iniciar Sesi&oacute;n</button>
                                    </div>
                                    <div class="hide-on-large-only col s12">
                                        <br>
                                        <div class="divider"></div>
                                        <br>
                                        <p>¿Aun no tienes una cuenta?</p>
                                        <p><a href="{{route('register')}}">Registrarme</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div id="modal-msg" class="modal">
                <div class="modal-content">
                    @if(Session::has('message'))
                    <h4 class="center-align blue-text text-darken-2">{{Session('title')}}</h4>
                    <p style="text-align: justify;">{{Session('message')}}</p>
                    @endif
                </div>
                <div class="modal-footer">
                    <button class="btn waves-effect waves-light blue darken-1 modal-close">Ok</button>
                </div>
            </div>
        </main>
        <footer class="blue-dark-meteor">
            <div class="footer-copyright">
                <div class="container white-text">
                    <div class="center-align">
                        @php
                        $year = date('Y');
                        @endphp
                        {{$year}}
                        <a class="grey-text text-lighten-4 right" href="#">“I do not think there is any thrill that can go through the human heart like that felt by the inventor as he sees some creation of the brain unfolding to success . . . Such emotions make a man forget food, sleep, friends, love, everything.” 
                            ― Nikola Tesla</a>
                    </div>            

                </div>
            </div>
        </footer>        
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('materialize/js/materialize.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/master.js')}}"></script>
    </body>
</html>