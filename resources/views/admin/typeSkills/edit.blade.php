@extends('layouts/master')
@section('title','MR - Editar habilidad')
@section('page_name','Editar habilidad')
@section('content')
<br>
<div class="row">
    <div class="col l6 offset-l3 s12 m6 offset-m3">
        <div class="card">
            <div class="card-content">
                <form method="POST" action="{{route('admin.type_skills.update')}}">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="input-field col l10 offset-l1 s12">
                            <input id="name" name="name" type="text" value="{{$typeSkill->name}}">
                            <label for="name">Nombre</label>                        
                            @foreach($errors->get('name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="col l10 s12 offset-l1 input-field">
                            <i id="icon_pref" class="material-icons prefix">burst_mode</i>
                            <input type="text" id="icon" name="icon" class="autocomplete" value="{{$typeSkill->icon}}">
                            <label for="icon">Icono</label>   
                            @foreach($errors->get('icon') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach 
                        </div>
                        <input type="hidden" name="id" id="id" value="{{$typeSkill->id}}">
                    </div>
                    <div class="right-align">
                        <a href="{{route('admin.type_skills')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/skills/autocompleteIcon.js')}}"></script>
@endsection