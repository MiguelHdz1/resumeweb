@extends('layouts/master')
@section('title','MR - Agregar habilidad')
@section('page_name','Agregar habilidad')
@section('content')
<br>
<div class="row">
    <div class="col l6 offset-l3 s12 m6 offset-m3">
        <div class="card">
            <div class="card-content">
                <form method="POST" action="{{route('admin.skills.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">                        
                        <div class="input-field col l10 s12 offset-l1">
                            <select id="type_skill" name="type_skill">
                                <option value="" disabled selected>Elige una opci&oacute;n</option>
                                @foreach($typeSkills as $typeSkill)
                                <option value="{{$typeSkill->id}}">{{$typeSkill->name}}</option>
                                @endforeach
                            </select>
                            <label>Categor&iacute;a</label>
                            @foreach($errors->get('type_skill') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach   
                        </div>
                        <div class="input-field col l10 offset-l1 s12">
                            <input id="name" name="name" type="text" value="{{old('name')}}">
                            <label for="name">Habilidad</label>                        
                            @foreach($errors->get('name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                    </div>
                    <div class="right-align">
                        <a href="{{route('admin.skills')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
@endsection