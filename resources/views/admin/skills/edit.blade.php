@extends('layouts/master')
@section('title','MR - Editar habilidad')
@section('page_name','Editar habilidad')
@section('content')
<br>
<div class="row">
    <div class="col l6 offset-l3 s12 m6 offset-m3">
        <div class="card">
            <div class="card-content">
                <form method="POST" action="{{route('admin.skills.update')}}" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf                    
                    <div class="row">                        
                        <div class="input-field col l10 s12 offset-l1">
                            <select id="type_skill" name="type_skill">
                                <option value="" disabled>Elige una opci&oacute;n</option>
                                @foreach($typeSkills as $typeSkill)
                                @if($typeSkill->id == $skill->id_type_skill)
                                <option selected value="{{$typeSkill->id}}">{{$typeSkill->name}}</option>
                                @else
                                <option value="{{$typeSkill->id}}">{{$typeSkill->name}}</option>
                                @endif
                                @endforeach
                            </select>
                            <label>Categor&iacute;a</label>
                            @foreach($errors->get('type_skill') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach   
                        </div>
                        <div class="input-field col l10 offset-l1 s12">
                            <input id="name" name="name" type="text" value="{{$skill->name}}">
                            <label for="name">Habilidad</label>                        
                            @foreach($errors->get('name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                    </div>
                    <input id="id" name="id" type="hidden" value="{{$skill->id}}">
                    <div class="right-align">
                        <a href="{{route('admin.skills')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
@endsection