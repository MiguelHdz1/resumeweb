@extends('layouts/master')
@section('title','MR - Crear red')
@section('page_name','Crear red')
@section('content')
<br>
<div class="row">
    <div class="col l6 offset-l3 s12 m6 offset-m3">
        <div class="card">
            <div class="card-content">
                <form method="POST" action="{{route('admin.network_types.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="input-field col l10 offset-l1 s12">
                            <input id="name" name="name" type="text" value="{{old('name')}}">
                            <label for="name">Nombre</label>                        
                            @foreach($errors->get('name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="file-field input-field col l10 offset-l1 s12">
                            <div class="btn teal darken-1">
                                <span>Icono</span>
                                <input type="file" name="icon" id="icon" value="{{old('icon')}}">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                            @foreach($errors->get('icon') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach   
                        </div>
                    </div>
                    <div class="right-align">
                        <a href="{{route('admin.network_types')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/networks/networks.js')}}"></script>
@endsection