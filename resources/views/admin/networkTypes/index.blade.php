@extends('layouts/master')
@section('title','MR - Redes')
@section('page_name','Redes')
@section('content')
<br>
<div class="row">
    <div class="col l4 m4 offset-m1 s6 offset-l1 offset-s1">

        <form method="GET" action="{{route('admin.network_types')}}">
            <div class="input-field">
                @if($button)
                <a class="prefix" href="/admin/network-types">
                    <i class="material-icons ">clear</i>
                </a>
                @endif
                <input id="search" name="search" type="text" value="{{$search}}">
                <label for="search">Buscar</label>
            </div>
        </form>
    </div>
    <div class="col m8 l6 s12 offset-l3 offset-m2 ">
        @if(sizeof($networks)!=0)
        <table class="striped centered">
            <thead>
            <th>Nombre</th>
            <th>Icono</th>
            <th>Acciones</th>
            </thead>
            <tbody>
                @foreach($networks as $network)
                <tr>
                    <td>
                        {{$network->name}}
                    </td>
                    <td>
                        <img class="responsive-img social-ico blue-dark-meteor" src="{{asset($network->icon)}}">
                    </td>
                    <td>
                        <a href="{{route('admin.network_types.edit',['id'=>$network->id])}}" class="btn waves-effect waves-light tooltipped green darken-1" data-position="top" data-tooltip="Editar">
                            <i class="material-icons">edit</i>
                        </a>                                                                        
                        <a id="btn-delete" href="" data-bind="{{$network->id}}" class="btn waves-effect waves-light tooltipped red darken-1" data-position="top" data-tooltip="Eliminar">
                            <i class="material-icons">delete</i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3">
                        <div class="center-align">
                            {{$networks->links('vendor/pagination/default')}}
                        </div>                        
                    </td>
                </tr>
            </tfoot>
        </table>
        @else
        <div class="center-align">
            <h5>No hay resultados</h5>
        </div>        
        @endif
    </div>
    <div class="fixed-action-btn">
        <a href="/admin/network-types/create" class="btn-floating btn-large red waves-effect waves-light pulse">
            <i class="large material-icons">add</i>
        </a>
    </div>
</div>
<form id="form_delete" method="POST" action="{{route('admin.network_types.delete')}}">
    @method('DELETE')
    @csrf
    <input type="hidden" id="id_delete" name="id_delete">
</form>
<div id="modal-delete" class="modal bottom-sheet">
    <div class="modal-content">
        <div class="row">
            <div class="col s12 l8 offset-l2">
                <h4>¿Deseas eliminar?</h4>
        <p>El eliminar este registro ya no permitira su uso, sin embargo sera visible para el perfil de los usuarios</p>
            </div>
        </div>        
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancelar</a>
        <a id="btn-delete-accept" href="#!" class="modal-close waves-effect waves-teal btn-flat">Aceptar</a>      
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/networks/networks.js')}}"></script>
@endsection