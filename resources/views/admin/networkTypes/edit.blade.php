@extends('layouts/master')
@section('title','MR - Editar red')
@section('page_name','Editar red')
@section('content')
<br>
<div class="row">
    <div class="col l6 offset-l3 s12 m6 offset-m3">
        <div class="card">
            <div class="card-content">
                <form method="POST" action="{{route('admin.network_types.update')}}" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="input-field col l10 offset-l1 s12">
                            <input id="name" name="name" type="text" value="{{$network->name}}">
                            <label for="name">Nombre</label>                        
                            @foreach($errors->get('name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="file-field input-field col l10 offset-l1 s12">
                            <div class="btn teal darken-1">
                                <span>Icono</span>
                                <input type="file" name="icon" id="icon" value="{{old('icon')}}">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" value="{{$network->icon}}">
                            </div>
                            @foreach($errors->get('icon') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach   
                        </div>
                        <input type="hidden" name="id" id="id" value="{{$network->id}}">
                    </div>
                    <div class="right-align">
                        <a href="{{route('admin.network_types')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
@endsection