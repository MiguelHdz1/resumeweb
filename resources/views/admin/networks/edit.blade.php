@extends('layouts/master')
@section('title','MR - Agregar red')
@section('page_name','Agregar red')
@section('content')
<br>
<div class="row">
    <div class="col l6 offset-l3 s12 m6 offset-m3">
        <div class="card">
            <div class="card-content">
                <form method="POST" action="{{route('admin.networks.update')}}">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="input-field col l10 offset-l1 s12">
                            <select id="id_type" name="id_type">
                                <option value="" disabled selected>Elige una red</option>
                                @foreach($networkTypes as $networkType)
                                @if($networkType->id == $network->id_type_network)
                                <option selected value="{{$networkType->id}}">{{$networkType->name}}</option>
                                @else
                                <option value="{{$networkType->id}}">{{$networkType->name}}</option>
                                @endif
                                @endforeach                                 
                            </select>
                            <label>Red</label>
                            @foreach($errors->get('id_type') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach
                        </div>
                        <div class="input-field col l10 offset-l1 s12">
                            <input id="url" name="url" type="url" value="{{$network->url}}">
                            <label for="url">URL</label>
                            @foreach($errors->get('url') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach
                        </div>                        
                    </div>
                    <input type="hidden" id="id" name="id" value="{{$network->id}}">
                    <div class="right-align">
                        <a href="{{route('admin.networks')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
@endsection