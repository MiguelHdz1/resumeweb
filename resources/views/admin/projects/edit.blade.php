@extends('layouts/master')
@section('title','MR - Editar proyecto')
@section('page_name','Editar proyecto')
@section('content')
<br>
<div class="row">
    <div class="col s12 l8 m6 offset-l2 offset-m3">
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <form method="post" action="{{route('admin.projects.update')}}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="col l10 s12 m10 offset-l1 offset-m1 input-field">
                            <input type="text" name="name" id="name" value="{{$project->name}}">
                            <label>Nombre</label>
                            @foreach($errors->get('name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach     
                        </div>
                        <div class="col l10 s12 m10 offset-l1 offset-m1 input-field">
                            <input type="text" name="url" id="url" value="{{$project->link}}">
                            <label>URL del proyecto (Opcional)</label>
                            @foreach($errors->get('url') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach     
                        </div>
                        <div class="file-field input-field col l10 s12 m10 offset-l1 offset-m1">
                            <div class="btn">
                                <span>Imagen</span>
                                <input type="file" name="image_project" id="image_project">
                            </div>
                            <div class="file-path-wrapper">
                                <input value="{{$project->image_url}}" id="img-name" name="img-name" class="file-path validate" type="text"  placeholder="(Opcional)">
                            </div>
                            @foreach($errors->get('image_project') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach 
                        </div>
                        <div class="input-field col l10 s12 m10 offset-l1 offset-m1">
                            <textarea id="description" name="description" class="materialize-textarea" data-length="500">{{$project->description}}</textarea>
                            <label for="description">Descripci&oacute;n</label>
                            @foreach($errors->get('description') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach 
                        </div>
                        <input type="hidden" id="id" name="id" value="{{$project->id}}">
                        <div class="right-align">
                        <a href="{{route('admin.projects')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection