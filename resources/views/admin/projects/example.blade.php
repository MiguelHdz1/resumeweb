@extends('layouts.master')
@section('content')
<br>
<div class="row">
    <div id="ddd" class="col l2">
      <div id="myEditor"></div>  
    </div>
</div>
@endsection
@section('javascript')
<script src="https://cdn.alloyui.com/3.0.1/aui/aui-min.js"></script>

<script>
    
    YUI().use(
  'aui-ace-editor',
  function(Y) {
    // code goes here
  }
);
YUI().use(
  'aui-ace-editor',
  function(Y) {
    new Y.AceEditor(
      {
        mode: 'php',
        boundingBox: '#myEditor',
        height: '200',
        width: 145*2
        
      }
    ).render();
  }
);
</script>
@endsection



<link rel="stylesheet"
      href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/styles/default.min.css">


<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.13.1/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
<pre>
<code class="php">    
    &lt;?php
    &lt;h1&gt;test&lt;/h1&gt;
    //hi
</code>
</pre>