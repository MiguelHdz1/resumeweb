@extends('layouts/master')
@section('title','MR - Proyectos')
@section('page_name','Proyectos')
@section('content')
<div class="row">
    <div class="col l4 offset-l1 s8 offset-s2 m5 offset-m1">        
        
            <div class="">
                <form method="GET" action="{{route('admin.projects')}}">
                    <div class="input-field">
                        @if($button)
                        <a class="prefix" href="/admin/projects">
                            <i class="material-icons ">clear</i>
                        </a>
                        @endif
                        <input id="search" name="search" type="text" value="{{$search}}">
                        <label for="search">Buscar</label>
                    </div>
                </form>
            </div>
    </div>
    <br><br><br><br>
    @if(sizeof($projects)>0)
    @foreach($projects as $project)
    <div class="col s12 l4 m6">
        <div class="card small">
            <div class="card-image waves-effect waves-block waves-light">
                @if($project->image_url != '')
                <img class="activator" src="{{asset($project->image_url)}}">
                @else
                <img class="activator" src="{{asset('images/project_default.jpg')}}">
                @endif
                <img class="activator" src="{{asset($project->image_url)}}">
                <span class="card-title">{{$project->name}}</span>
            </div>
            <div class="card-content">
                <i class="material-icons right activator">more_vert</i>
                <p>{{$project->description}}</p>
            </div>
            <div class="card-action">
                <a data-bind="{{$project->id}}" href="#!" class="btn-delete waves-effect waves-meteor tooltipped" data-position="top" data-tooltip="Eliminar">
                    <i class="material-icons grey-text text-darken-3">delete</i>
                </a>
                <a href="{{route('admin.projects.edit',['id'=>$project->id])}}" class="waves-effect waves-meteor tooltipped" data-position="top" data-tooltip="Editar">
                    <i class="material-icons grey-text text-darken-3">edit</i>
                </a>
                <a href="{{route('admin.project_sections',['id'=>$project->id])}}" class="waves-effect waves-meteor tooltipped" data-position="top" data-tooltip="Secciones">
                    <i class="material-icons grey-text text-darken-3">format_list_numbered</i>
                </a>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">{{$project->name}}<i class="material-icons right">close</i></span>
                <p>{{$project->description}}</p>
            </div>
        </div>
    </div>
    @endforeach
    <div id="div-more" class="center-align col s12">
        {{$projects->links('vendor.pagination.default')}}
    </div>
    @else
    <h5 class="center-align">No hay resultados</h5>
    @endif
    <div class="fixed-action-btn">
        <a href="/admin/projects/create" class="btn-floating btn-large red waves-effect waves-light pulse">
            <i class="large material-icons">add</i>
        </a>
    </div>
</div>
<form id="form_delete" method="POST" action="{{route('admin.projects.delete')}}">
    @method('DELETE')
    @csrf
    <input type="hidden" id="id_delete" name="id_delete">
</form>
<div id="modal-delete" class="modal bottom-sheet">
    <div class="modal-content">
        <div class="row">
            <div class="col s12 l8 offset-l2">
                <h4>¿Deseas eliminar?</h4>
                <p>El eliminar este registro ya no permitira su uso, sin embargo sera visible para el perfil de los usuarios</p>
            </div>
        </div>        
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-red btn-flat">Cancelar</a>
        <a id="btn-delete-accept" href="#!" class="modal-close waves-effect waves-teal btn-flat">Aceptar</a>      
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/projects/projects.js')}}"></script>
@endsection