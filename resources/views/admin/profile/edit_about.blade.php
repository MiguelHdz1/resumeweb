@extends('layouts/master')
@section('title','MR - Editar "Acerca de mi"')
@section('page_name','Editar "Acerca de mi"')
@section('content')
<br>
<div class="row">
    <div class="col l6 offset-l3 s12 m6 offset-m3">
        <div class="card">
            <div class="card-content">                
                <form method="POST" action="{{route('admin.profile.aboutme.edit')}}">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="about_me" name="about_me" class="materialize-textarea" data-length="500">{{$about->about_me}}</textarea>
                            <label for="about_me">Escribe acerca de ti...</label>
                            @foreach($errors->get('about_me') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach    
                        </div>
                        <input type="hidden" name="id" id="id" value="{{$about->id}}">
                    </div>
                    <div class="right-align">
                        <a href="{{route('admin')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/skills/autoCompleteIcon.js')}}"></script>
@endsection