@extends('layouts/master')
@section('title','MR - Editar información personal')
@section('page_name','Información personal')
@section('content')
<br>
<div class="row">
    <div class="col l6 offset-l3 s12 m6 offset-m3">
        <div class="card">
            <div class="card-content">                
                <form method="POST" action="{{route('admin.profile.personalInfo.edit')}}">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="profession" name="profession" value="{{$profile->profession}}">
                            <label for="profession">Profesi&oacute;n</label>
                            @foreach($errors->get('profession') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach    
                        </div>
                        <div class="input-field col s12">
                            <input type="text" id="current_job" name="current_job" value="{{$profile->current_job}}">
                            <label for="about_me">Puesto actual</label>
                            @foreach($errors->get('current_job') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach    
                        </div>
                        <div class="input-field col s12">
                            <input type="text" class="datepicker" id="birthday" name="birthday" value="{{$profile->birthday}}">
                            <label for="birthday">Fecha de naciemiento</label>
                            @foreach($errors->get('birthday') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach    
                        </div>
                        <div class="input-field col s12">
                            <input type="text" id="lives_in" name="lives_in" value="{{$profile->lives_in}}">
                            <label for="about_me">Tu localidad</label>
                            @foreach($errors->get('lives_in') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach    
                        </div>
                        <input type="hidden" name="id" id="id" value="{{$profile->id}}">
                    </div>
                    <div class="right-align">
                        <a href="{{route('admin')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/skills/profile.js')}}"></script>
@endsection