@extends('layouts/master')
@section('title','MR - Editar presentación')
@section('page_name','Editar presentación')
@section('css')
<link href="{{asset('css/admin/profile/presentation.css')}}" rel="stylesheet">   
@endsection
@section('content')
<br>
<div class="row ">
    <form method="POST" action="{{route('admin.profile.presentation.edit')}}" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="col s12 m10 l6 offset-m1">
            <div class="card">                        
                <div class="card-content">
                    <div class="card-title">Imagen de perfil</div>
                    <div class="row">
                        <div class="col l6 m6 s6 offset-s3">
                            <img id="img_profile" class="responsive-img image-edit" src="{{asset(Auth::user()->avatar)}}">
                        </div>
                        <div class="col l6 m6 s12">
                            <div class="file-field input-field">
                                <div class="btn">
                                    <span>Imagen</span>
                                    <input id="file_profile" name="file_profile" type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" value="{{$profile->avatar}}">
                                </div>
                                @foreach($errors->get('file_profile') as $error)
                                <span class="label-error"> {{$error}}</span>
                                @endforeach    
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>                                
            <div class="card">
                <div class="card-content">
                    <div class="card-title">Imagen de portada</div>
                    <div class="row">
                        <div class="col l6 m6 s6 offset-s3">
                            <img id="img_back" class="responsive-img image-edit" src="{{asset(Auth::user()->background)}}">
                        </div>
                        <div class="col l6 m6 s12">
                            <div class="file-field input-field">
                                <div class="btn">
                                    <span>Imagen</span>
                                    <input id="file_back" name="file_back" type="file">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" value="{{$profile->background}}">
                                </div>
                                @foreach($errors->get('file_back') as $error)
                                <span class="label-error"> {{$error}}</span>
                                @endforeach 
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>                            
        </div>
        <div class="col s12 m10 l6 offset-m1">
            <div class="card">
                <div class="card-content">
                    <div class="card-title">Presentaci&oacute;n</div>
                    <div class="row">
                        <div class="input-field col l12 s12">
                            <input id="name" name="name" type="text" value="{{$profile->name}}">
                            <label for="name">Nombre(s)</label>
                            @foreach($errors->get('name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="input-field col l6 s6">
                            <input id="first_name" name="first_name" type="text" value="{{$profile->first_name}}">
                            <label for="first_name">Apellido paterno</label>                        
                            @foreach($errors->get('first_name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="input-field col l6 s6">
                            <input id="last_name" name="last_name" type="text" value="{{$profile->last_name}}">
                            <label for="last_name">Apellido materno</label>
                            @foreach($errors->get('last_name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="input-field col l12 s12">
                            <input id="phone" name="phone" type="number" value="{{$profile->phone_number}}">
                            <label for="phone">Tel&eacute;fono</label>
                            @foreach($errors->get('phone') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="input-field col l6 s6">
                            <input id="password" name="password" type="password">
                            <label for="password">Contrase&ntilde;a</label>                        
                            @foreach($errors->get('password') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div>
                        <div class="input-field col l6 s6">
                            <input id="password_confirmation" name="password_confirmation" type="password">
                            <label for="password_confirmation">Repetir contrase&ntilde;a</label>                        
                            @foreach($errors->get('password_confirmation') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach                        
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <div class="right-align col s12">
            <a href="{{route('admin')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
            <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>                    
        </div>
    </form>
</div>
<div class="fixed-action-btn">
    <a id="message" class="btn btn-floating btn-large darken-2 green"><i class="material-icons">message</i></a>
</div>

<div class="tap-target darken-2 green" data-target="message">
    <div class="tap-target-content white-text">
        <h4>Nota</h4>
        <p class="white-text">La contraseña y las imagenes del usuario no cambiaran a menos que estas sean editadas.</p>
    </div>
</div>

@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/profile/presentation.js')}}"></script>
@endsection