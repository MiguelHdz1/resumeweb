@extends('layouts/public')
@section('title','MR - Secciones')
@section('page_name','Secciones')
@section('avatar',asset($profile->avatar))
@section('background',asset($profile->background))
@section('name',$profile->name.' '.$profile->first_name.' '.$profile->last_name)
@section('routing',$profile->user_link)
@section('email',$profile->email)
@section('css')
<link href="{{asset('css/admin/projects/sections.css')}}" rel="stylesheet">   
@endsection
@section('content')
<div class="row">
    <div class="col s12">
        <div class="card tinted-image sizing-back ">
            <div class="card-content white-text">
                <span class="card-title">{{$project->name}}</span>
                <p>{{$project->description}}</p>
            </div>
            @if($project->link !='')
            <div class="card-action right-align">
                <a class="btn btn-small waves-effect waves-light blue darken-1" target="_blank" href="{{$project->link}}">Repositorio</a>                
            </div>
            @endif
            <div class="image_back" data-bind="{{asset($project->image_url)}}"></div>
        </div>
    </div>
    @if(sizeof($sections)>0)

    @foreach($sections as $section)
    <div class="col s12">
        <div class="block ">
            <div class="divider"></div>
            <br>
            <div class="row">                
                <div class="col l7 s12 m10 offset-m1">
                    <img class="responsive-img materialboxed img-sections center-block" src="{{asset($section->picture)}}">
                </div>
                <div class="col l5 s12 m10 offset-m1">
                    <h5>{{$section->title}}</h5>
                    <p align="justify">{{$section->description}}</p>
                </div>
            </div>
        </div>

    </div>
    @endforeach

    @else
    <div class="col s12 center-align">
        <h5>Este proyecto no contiene secciones a&uacute;n</h5>
    </div>
    @endif
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/projects/sections.js')}}"></script>
<script type="text/javascript" src="{{asset('js/admin/projects/projects.js')}}"></script>
@endsection