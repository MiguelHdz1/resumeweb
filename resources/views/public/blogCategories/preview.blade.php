@extends('layouts/master')
@section('title','MR - Vista previa')
@section('page_name','Vista previa')
@section('css')
<link href="{{asset('js/admin/trumbowyg/dist/ui/trumbowyg.min.css')}}" rel="stylesheet">
<link href="{{asset('css/admin/projects/sections.css')}}" rel="stylesheet">   
@endsection
@section('content')
<div class="row">
    <div class="col s12">
        <div class="card tinted-image sizing-back">
            <div class="card-content white-text">
                <span class="card-title">{{$blog->title}}</span>
                <span>{{$blog->description}}</span><br>
                <span>Publicado el dia {{$blog->date}}</span>
            </div>
        </div>
    </div>
    <div class="col s12">
        <div class="show">
            {!!$blog->content!!}
        </div>
    </div>
</div>
<input type="hidden" class="image_back" data-bind="{{asset($blog->url_cover)}}">
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/blog/index.js')}}"></script>
@endsection