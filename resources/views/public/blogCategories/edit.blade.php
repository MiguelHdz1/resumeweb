@extends('layouts/master')
@section('title','MR - Editar categoría(Blog)')
@section('page_name','Editar categoría (Blog)')
@section('content')
<br>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <form method="POST" action="{{route('admin.blog_categories.update')}}">
                        @csrf
                        @method('PUT')
                        <div class="input-field col s12 l6 m6">
                            <input id="name" name="name" type="text" value="{{$categorie->name}}">
                            <label for="name">Nombre</label>
                            @foreach($errors->get('name') as $error)
                            <span class="label-error"> {{$error}}</span>
                            @endforeach   
                        </div>
                        <div class=" col s12">
                            <div class="right-align">
                                <a href="{{route('admin.blog_categories')}}" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                                <button type="submit" class="btn waves-effect waves-light blue darken-3">Guardar</button>
                            </div>
                        </div>
                        <input type="hidden" id="id" name="id" value="{{$categorie->id}}">
                    </form>  
                </div>
            </div>
        </div>
    </div>  
</div>
@endsection