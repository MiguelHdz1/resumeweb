<!DOCTYPE html>
<html>
    <head>        
        <title>MR - Resgitrarme</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="icon" href="{{url('images/kisspng.png')}}">
        <link href="{{asset('css/admin/login/login.css')}}" rel="stylesheet">
        <link href="{{asset('css/master.css')}}" rel="stylesheet">        
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{asset('materialize/css/materialize.min.css')}}" rel="stylesheet">
    </head>
    @if(Session::has('message'))
    <div class="message" id="{{Session('type')}}" data-bind="{{Session('message')}}">            
    </div>
    @endif
    <body>
        <nav>
            <div class="nav-wrapper blue-meteor">
                <a class="brand-logo center">Registrarme</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a class="btn waves-effect waves-light orange darken-4" href="/">
                            <i class="material-icons left">lock</i>
                            Iniciar sesi&oacute;n
                        </a>
                    </li>
                </ul>
            </div>
        </nav>        
        <main>
            <div class="row">
                <div class="col s12 l6 m8 offset-l3 offset-m2">
                    <form method="POST" action="{{route('register.done')}}">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-title blue-text text-darken-2">Informaci&oacute;n personal</div>
                                <div class="row">
                                    <div class="input-field col l6 s12">
                                        <input type="text" id="name" name="name" value="{{old('name')}}">
                                        <label for="name">Nombre(s)</label>
                                        @foreach($errors->get('name') as $error)
                                        <span class="label-error"> {{$error}}</span>
                                        @endforeach             
                                    </div>    
                                    <div class="input-field col l6 s12">
                                        <input type="text" id="first_name" name="first_name" value="{{old('first_name')}}">
                                        <label for="first_name">Apellido paterno</label>
                                        @foreach($errors->get('first_name') as $error)
                                        <span class="label-error"> {{$error}}</span>
                                        @endforeach             
                                    </div>    
                                    <div class="input-field col l6 s12">
                                        <input type="text" id="last_name" name="last_name" value="{{old('last_name')}}">
                                        <label for="last_name">Apellido materno</label>
                                        @foreach($errors->get('last_name') as $error)
                                        <span class="label-error"> {{$error}}</span>
                                        @endforeach             
                                    </div>    
                                </div>
                            </div>
                            @csrf
                            <div class="card-content grey lighten-3">
                                <div class="card-title blue-text text-darken-2">Informaci&oacute;n de usuario</div>
                                <div class="row">                                
                                    <div class="input-field col l12 s12">
                                        <input type="email" id="email" name="email" value="{{old('email')}}">
                                        <label for="email">Email</label>
                                        @foreach($errors->get('email') as $error)
                                        <span class="label-error"> {{$error}}</span>
                                        @endforeach             
                                    </div> 
                                    <div class="input-field col l6 s12">
                                        <input type="password" id="password" name="password">
                                        <label for="password">Contrase&ntilde;a</label>
                                        @foreach($errors->get('password') as $error)
                                        <span class="label-error"> {{$error}}</span>
                                        @endforeach             
                                    </div>
                                    <div class="input-field col l6 s12">
                                        <input type="password" id="password_confirmation" name="password_confirmation">
                                        <label for="password_confirmation">Confirmar contrase&ntilde;a</label>
                                        @foreach($errors->get('password_confirmation') as $error)
                                        <span class="label-error"> {{$error}}</span>
                                        @endforeach             
                                    </div>
                                    <div class="right-align col s12">
                                        <a href="/" class="btn waves-effect waves-light red darken-2">Cancelar</a>
                                        <button type="submit" class="btn waves-effect waves-light blue darken-3">Listo</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>            
        </main>
        <footer class="blue-dark-meteor">
            <div class="footer-copyright">
                <div class="container white-text">
                    <div class="center-align">
                        @php
                        $year = date('Y');
                        @endphp
                        {{$year}}
                        <a class="grey-text text-lighten-4 right" href="#">“I do not think there is any thrill that can go through the human heart like that felt by the inventor as he sees some creation of the brain unfolding to success . . . Such emotions make a man forget food, sleep, friends, love, everything.” 
                            ― Nikola Tesla</a>
                    </div>            

                </div>
            </div>
        </footer>        
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('materialize/js/materialize.min.js')}}"></script>       
        <script type="text/javascript" src="{{asset('js/master.js')}}"></script>
    </body>
</html>