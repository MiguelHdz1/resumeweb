@extends('layouts/public')
@section('title','MR - Blog')
@section('page_name','Blog')
@section('content')
@section('avatar',asset($profile->avatar))
@section('background',asset($profile->background))
@section('name',$profile->name.' '.$profile->first_name.' '.$profile->last_name)
@section('routing',$profile->user_link)
@section('email',$profile->email)
@section('css')
<link href="{{asset('css/admin/blog/index.css')}}" type="text/css" rel="stylesheet">
@endsection
<div class="row">
    <div class="col l4 offset-l1 s8 offset-s2 m5 offset-m1">        

        <div class="">
            <form method="GET" action="/{{$profile->user_link}}/blog">
                <div class="input-field">
                    @if($button)
                    <a class="prefix" href="/{{$profile->user_link}}/blog">
                        <i class="material-icons ">clear</i>
                    </a>
                    @endif
                    <input id="search" name="search" type="text" value="{{$search}}">
                    <label for="search">Buscar</label>
                </div>
            </form>
        </div>
    </div>
    <br><br><br><br>
    @if(sizeof($blogs))
    @foreach($blogs as $blog)
    <div class="col s12 l4 m6 white-text">
        <div class="card small card-smallest light-blue darken-2 tinted-image" style="background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.65)), url(&quot;{{asset($blog->url_cover)}}&quot;);">
            <div class="card-content">
                <div class="card-title">{{$blog->title}}</div>
                <p>{{$blog->description}}</p>
            </div>
            <div class="card-action light-blue darken-3">
                <a class="white-text">{{$blog->date}}</a>                                                           
                <a href="/{{$profile->user_link}}/blog/{{$blog->id}}/view" class="white-text right tooltipped" data-position="top" data-tooltip="Leer">
                    <i class="material-icons">bookmark_border</i>
                </a>                                                                                            
            </div>
        </div>
    </div>
    @endforeach
    <div id="div-more" class="center-align col s12">
        {{$blogs->links('vendor.pagination.default')}}
    </div>
    @else
    <br>
    <div class="col s8 offset-s2 center-align">
        <h5>Aun no existe alg&uacute;n blog</h5>
    </div>
    @endif    
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/blog/index.js')}}"></script>
@endsection