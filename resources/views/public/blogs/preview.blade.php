@extends('layouts/public')
@section('title','MR - Blog')
@section('page_name','Blog')
@section('avatar',asset($profile->avatar))
@section('background',asset($profile->background))
@section('name',$profile->name.' '.$profile->first_name.' '.$profile->last_name)
@section('routing',$profile->user_link)
@section('email',$profile->email)
@section('css')
<link href="{{asset('js/admin/trumbowyg/dist/ui/trumbowyg.min.css')}}" rel="stylesheet">
<link href="{{asset('css/admin/projects/sections.css')}}" rel="stylesheet">   
@endsection
@section('content')
<div class="row">
    <div class="col s12">
        <div class="card tinted-image sizing-back" style="background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.65)), url(&quot;{{asset($blog->url_cover)}}&quot;);">
            <div class="card-content white-text">
                <span class="card-title">{{$blog->title}}</span>
                <span>{{$blog->description}}</span><br>
                <span>Published: {{$blog->date}}</span>
            </div>
        </div>
    </div>
    <div class="col s12">
        <div class="show">
            {!!$blog->content!!}
        </div>
    </div>
</div>
<input type="hidden" class="image_back" data-bind="{{asset($blog->url_cover)}}">
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/blog/index.js')}}"></script>
@endsection