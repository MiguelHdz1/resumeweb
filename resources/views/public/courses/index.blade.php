@extends('layouts/public')
@section('title','MR - Cursos/Certificados')
@section('page_name','Cursos/Certificados')
@section('avatar',asset($profile->avatar))
@section('background',asset($profile->background))
@section('name',$profile->name.' '.$profile->first_name.' '.$profile->last_name)
@section('routing',$profile->user_link)
@section('email',$profile->email)
@section('css')
<link href="{{asset('css/admin/blog/index.css')}}" type="text/css" rel="stylesheet">
@endsection
@section('content')
<br>
<div class="row">
    <div class="col l4 m4 offset-m1 s6 offset-l1 offset-s1">

        <form method="GET" action="{{route('public.courses',['user'=>$profile->user_link])}}">
            <div class="input-field">
                @if($button)
                <a class="prefix" href="{{route('public.courses',['user'=>$profile->user_link])}}">
                    <i class="material-icons ">clear</i>
                </a>
                @endif
                <input id="search" name="search" type="text" value="{{$search}}">
                <label for="search">Buscar</label>
            </div>
        </form>
    </div>
    <br>
    <br>
    <br>
    <br>
    @if(sizeof($courses)!=0)
    @foreach($courses as $course)
    <div class="col m6 l6 s12">
        <div class="card {{$colors[array_rand($colors,1)]}} small card-smallest">
            <div class="card-content white-text">                
                <span class="card-title">{{$course->name}}</span>
                <p>Otorga: {{$course->institute_name}}</p>                
                @if($course->file_type == '0')
                <p>Tipo: Certificaci&oacute;n</p>
                @elseif($course->file_type =='1')
                <p>Tipo: Curso</p>
                @else
                <p>Tipo: Otro</p>
                @endif
                <p>Licencia: 
                    @if($course->license !='')
                    {{$course->license}}
                    @else
                    N/A
                    @endif
                </p>                
            </div>
            <div class="card-action">                            
                <a href="{{route('public.courses.show',['id'=>$course->id,'user'=>$profile->user_link])}}" class=" white-text waves-effect waves-light right">
                    ver
                </a>
            </div>
        </div>
    </div>    
    @endforeach    
    <div class="col s12 center-align">
        {{$courses->links('vendor.pagination.default')}}
    </div>
    @else
    <div class="center-align">
        <h5>No hay resultados</h5>
    </div>        
    @endif
</div>
@endsection