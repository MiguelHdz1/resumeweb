@extends('layouts/public')
@section('title','MR - Cursos/Certificados')
@section('page_name','Cursos/Certificados')
@section('avatar',asset($profile->avatar))
@section('background',asset($profile->background))
@section('name',$profile->name.' '.$profile->first_name.' '.$profile->last_name)
@section('routing',$profile->user_link)
@section('email',$profile->email)
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/admin/courses/show.css')}}">
@endsection
@section('content')
<br>
<div class="row">
    <br>
    <div class="col s12 l10 offset-l1 m10 offset-m1">
        <ul class="collection with-header">
            <li class="collection-header "><h4 class="center">{{$course->name}} 
                    @if($course->file_type == '0')
                    (Certificaci&oacute;n)
                    @elseif($course->file_type =='1')
                    (Curso)
                    @else
                    (Otro)
                    @endif</h4></li>
            <li class="collection-item">            
                <p>
                    <i class="material-icons left">message</i>
                    {{$course->description}}
                </p>
            </li>
            <li class="collection-item">            
                <p>
                    <i class="material-icons left">verified_user</i>
                    Licencia: {{$course->license}}
                </p>
            </li>
            <li class="collection-item">            
                <p>
                    <i class="material-icons left">business</i>
                    Certifica: {{$course->institute_name}}
                </p>
            </li>            
            <li class="collection-item center-align">            
                <p>
                    <i class="material-icons left">picture_as_pdf</i>
                    <br><br>
                    <iframe class="center pdfShow" style="display: flex; height: 70vh" src="{{asset('js/ViewerJS/#../..')}}/{{$course->file_url}}" allowfullscreen webkitallowfullscreen></iframe>
                </p>
            </li>            
        </ul>
    </div>
    
</div>
<div class="row">
    <div class="">
        
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/ViewerJS/pdf.js')}}"></script>
@endsection