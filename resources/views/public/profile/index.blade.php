@extends('layouts/public')
@section('title','MR - Perfil')
@section('page_name','Perfil')
@section('avatar',asset($profile->avatar))
@section('background',asset($profile->background))
@section('name',$profile->name.' '.$profile->first_name.' '.$profile->last_name)
@section('routing',$profile->user_link)
@section('email',$profile->email)
@section('css')
<link href="{{asset('css/admin/profile/profile.css')}}" rel="stylesheet">  
<link href="{{asset('css/livepreview-demo.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-image">                   
                <img class="center card-img-head light-blue darken-3" src="{{asset($profile->background)}}">                               
                <a class="btn-floating button-profile halfway-fab left waves-effect waves-light blue-grey darken-4">
                    <img class="center image-profile z-depth-2" src="{{asset($profile->avatar)}}">
                </a>               
            </div>
            <div class="divider"></div>
            <div class="card-content">                
                <div class="row">
                    <div class="col l6 s7 m6">
                        <label class="label-info">
                            <i class="material-icons">account_circle</i>
                            {{' '.$profile->name.' '.$profile->first_name.' '.$profile->last_name}}
                        </label>
                    </div>
                    <div class="col l6 s5  m6">
                        <label class="label-info"><i class="material-icons">phone</i> {{$profile->phone_number}}</label>
                    </div>  
                    <div class="col l6 s12  m6">
                        <label class="label-info"><i class="material-icons">email</i>{{' '.$profile->email}}</label>
                    </div>
                    <div class="col l6 s5  m6">
                        <label class="label-info"><i class="material-icons">language</i> {{$profile->user_link}}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12">
        <div class="row">
            <div class="col s12 l7 m7">
                <ul class="collapsible">
                    <li class="center blue-light-meteor">
                        <div class="card-title white-text collapsible-no-pointer blue-light-meteor">                            
                            <span class="center">Informaci&oacute;n personal</span>
                        </div>                        
                    </li>
                    <li>                        
                        <div class="collapsible-header collapsible-no-pointer"><i class="material-icons">school</i>Profesi&oacute;n: {{$profile->profession}}</div>                                
                    </li>
                    <li>
                        <div class="collapsible-header collapsible-no-pointer"><i class="material-icons">work</i>Puesto actual: {{$profile->current_job}}</div>                                
                    </li>
                    <li>
                        <div class="collapsible-header collapsible-no-pointer"><i class="material-icons">cake</i>Fecha de nacimiento: {{$profile->birthday}}</div>                                
                    </li>
                    <li>
                        <div class="collapsible-header collapsible-no-pointer"><i class="material-icons">location_city</i>De: {{$profile->lives_in}}</div>                                
                    </li>           
                    <li class="right-align">
                        <div class="white-text collapsible-no-pointer blue-light-meteor">                            
                            <br>
                        </div>                        
                    </li>
                </ul>

                <ul class="collapsible expandable">
                    <li class="center blue-light-meteor">
                        <div class="card-title white-text collapsible-no-pointer blue-light-meteor">                            
                            <span class="center">Experiencia</span></div>                        
                    </li>
                    @foreach($experiences as $experience)
                    <li>
                        @if($experience->current == '1')
                        <div class="collapsible-header teal-text">
                            <i class="material-icons">business</i>{{$experience->company}} (Actual)
                            <i class="material-icons">expand_more</i>
                        </div>
                        <div class="collapsible-body teal-text">
                            <span>Del {{$experience->initial_date}} - Actual</span>
                            <p>{{$experience->about}}</p>
                        </div>
                        @else
                        <div class="collapsible-header">
                            <i class="material-icons">business</i>{{$experience->company}}
                            <i class="material-icons">expand_more</i>
                        </div>
                        <div class="collapsible-body">
                            <span>Del {{$experience->initial_date}} - {{$experience->final_date}}</span>
                            <p>{{$experience->about}}</p>
                        </div>
                        @endif                        
                    </li>
                    @endforeach                                        
                    <li class="right-align">
                        <div class="white-text collapsible-no-pointer blue-light-meteor">
                            <br>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col s12 l5 m5">
                <div class="card blue-light-meteor">
                    <div class="card-content">
                        <span class="card-title white-text">Acerca de mi!</span>
                        <p class="white-text">{{$profile->about_me}}</p>
                    </div>
                    <div class="right-align blue-meteor">
                        <br>
                    </div>                    
                </div>
                <ul class="collapsible">
                    <li class="center">
                        <div class="card-title white-text collapsible-no-pointer blue-meteor">                            
                            <span class="center">Redes</span></div>                        
                    </li>
                    @foreach($networks as $network)
                    <li>
                        <div class="collapsible-header collapsible-no-pointer blue-light-meteor white-text">
                            <i class="material-icons">
                                <img class="social-ico responsive-img" src="{{asset($network->icon)}}">                           
                            </i>
                            <a target="_blank" class="livepreview" href="{{$network->full_url}}">{{$network->url}}</a>
                        </div>                                
                    </li>                                                       
                    @endforeach
                    <li class="right-align">
                        <div class="white-text collapsible-no-pointer blue-meteor">                            
                            <br>
                        </div>                        
                    </li>
                </ul>
                <ul class="collapsible expandable">
                    <li class="center">
                        <div class="card-title white-text collapsible-no-pointer blue-meteor">                            
                            <span class="center">Habilidades</span>
                        </div>                        
                    </li>
                    @foreach($skills as $skill)
                    <li>
                        <div class="collapsible-header blue-light-meteor white-text">
                            <i class="material-icons">{{$skill['icon']}}</i>{{$skill['name']}}
                            <i class="material-icons right">expand_more</i>                                        
                        </div>
                        <div class="collapsible-body blue-light-meteor-2">
                            <div class="row collapsible-marginless">
                                <ol>
                                    @foreach($skill['current_skills'] as $new_skill)
                                    <div class="col s6">
                                        <li class="white-text">{{$new_skill->name}}</li>
                                    </div>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                    </li>
                    @endforeach
                    <li class="right-align">
                        <div class="white-text collapsible-no-pointer blue-meteor">
                            <br>
                        </div>
                    </li>
                </ul>
            </div>            
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/profile/profile.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery-live-preview.min.js')}}"></script>
@endsection