@extends('layouts/public')
@section('title','MR - Proyectos')
@section('page_name','Proyectos')
@section('avatar',asset($profile->avatar))
@section('background',asset($profile->background))
@section('name',$profile->name.' '.$profile->first_name.' '.$profile->last_name)
@section('routing',$profile->user_link)
@section('email',$profile->email)
@section('content')
<div class="row">
    <div class="col l4 offset-l1 s8 offset-s2 m5 offset-m1">                
            <div class="">
                <form method="GET" action="/{{$profile->user_link}}/projects">
                    <div class="input-field">
                        @if($button)
                        <a class="prefix" href="/{{$profile->user_link}}/projects">
                            <i class="material-icons ">clear</i>
                        </a>
                        @endif
                        <input id="search" name="search" type="text" value="{{$search}}">
                        <label for="search">Buscar</label>
                    </div>
                </form>
            </div>
    </div>
    <br><br><br><br>
    @if(sizeof($projects)>0)
    @foreach($projects as $project)
    <div class="col s12 l4 m6">
        <div class="card small">
            <div class="card-image waves-effect waves-block waves-light">
                @if($project->image_url != '')
                <img class="activator" src="{{asset($project->image_url)}}">
                @else
                <img class="activator" src="{{asset('images/project_default.jpg')}}">
                @endif
                <img class="activator" src="{{asset($project->image_url)}}">
                <span class="card-title">{{$project->name}}</span>
            </div>
            <div class="card-content">
                <i class="material-icons right activator">more_vert</i>
                <p>{{$project->description}}</p>
            </div>
            <div class="card-action right-align">
                <a href="{{route('public.project_sections',['id'=>$project->id,'user'=>$profile->user_link])}}" class="waves-effect waves-meteor tooltipped" data-position="top" data-tooltip="Secciones">
                    <i class="material-icons grey-text text-darken-3">format_list_numbered</i>
                </a>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">{{$project->name}}<i class="material-icons right">close</i></span>
                <p>{{$project->description}}</p>
            </div>
        </div>
    </div>
    @endforeach
    <div id="div-more" class="center-align col s12">
        {{$projects->links('vendor.pagination.default')}}
    </div>
    @else
    <h5 class="center-align">No hay resultados</h5>
    @endif
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/admin/projects/projects.js')}}"></script>
@endsection