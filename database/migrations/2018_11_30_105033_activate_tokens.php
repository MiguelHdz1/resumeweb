<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivateTokens extends Migration
{
    
    public function up()
    {
        Schema::create('activate_tokens',function(Blueprint $table){
            $table->increments('id');
            $table->string('token',191);
            $table->unsignedInteger('id_user');
            $table->unique('token');
            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('activate_tokens');
    }
}
