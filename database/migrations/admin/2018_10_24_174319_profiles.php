<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Profiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function(Blueprint $table){
            $table->increments('id')->nullable();
            $table->string('name',150)->nullable(false);
            $table->string('first_name',100)->nullable(false);
            $table->string('last_name',100)->nullable(false);
            $table->char('gender',1)->nullable(true);
            $table->dateTime('birthday')->nullable(true);
            $table->string('phone_number',10)->nullable(true);
            $table->string('avatar',100)->nullable(true);
            $table->string('background',100)->nullable(true);
            $table->string('about_me',500)->nullable(true);
            $table->string('profession',150)->nullable(true);
            $table->string('current_job',150)->nullable(true);
            $table->string('lives_in',150)->nullable(true);
            $table->unsignedInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
