<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects',function(Blueprint $table){
            $table->string('image_url',100)->after('link')->nullable(true);
        });
    }

    public function down()
    {
        Schema::table('projects',function(Blueprint $table){
            $table->dropColumn('image_url');
        });
    }
}
