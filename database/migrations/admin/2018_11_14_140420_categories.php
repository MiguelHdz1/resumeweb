<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Categories extends Migration
{
    
    public function up()
    {
        Schema::create('blog_categories',function(Blueprint $table){
            $table->increments('id');
            $table->string('name',30);
            $table->char('status',1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('blog_categories');
    }
}
