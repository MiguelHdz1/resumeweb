<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Experience extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences',function(Blueprint $table){
            $table->increments('id')->nullable(false);
            $table->string('company',50)->nullable(false);
            $table->dateTime('initial_date')->nullable(false);
            $table->dateTime('final_date')->nullable(false);
            $table->string('about',500)->nullable(false);            
            $table->unsignedInteger('id_user')->nullable(false);            
            $table->foreign('id_user')->references('id')->on('users');                        
            $table->timestamps();                        
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('experiences');
    }
}
