<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Courses extends Migration{
    
    public function up(){
        Schema::create('courses',function(Blueprint $table){
            $table->increments('id')->nullable(false);
            $table->string('name',100)->nullable(false);
            $table->string('institute_name',150)->nullable(false);
            $table->string('description',700)->nullable(false);
            $table->char('file_type',1)->nullable(false);
            $table->string('file_url','200')->nullable(true);
            $table->string('license')->nullable(true);
            $table->unsignedInteger('id_user')->nullable(true);
            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamps();            
        });
    }

    public function down(){
        Schema::dropIfExists('courses');
    }
}
