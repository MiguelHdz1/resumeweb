<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Blogs extends Migration
{
    public function up(){
        Schema::create('blogs',function(Blueprint $table){
            $table->increments('id');
            $table->string('title',60);
            $table->mediumtext('content',70500);
            $table->string('url_cover',200)->nullable(true);
            $table->string('description',150);
            $table->unsignedInteger('id_categorie');
            $table->unsignedInteger('id_user');
            $table->foreign('id_categorie')->references('id')->on('blog_categories');
            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
