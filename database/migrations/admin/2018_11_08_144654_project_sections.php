<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProjectSections extends Migration
{
    
    public function up(){
        Schema::create('project_sections',function(Blueprint $table){
            $table->increments('id')->nullable(false);
            $table->string('title',100)->nullable(false);
            $table->string('description',500)->nullable(false);
            $table->string('picture',100)->nullable(true);
            $table->unsignedInteger('id_project')->nullable(false);
            $table->foreign('id_project')->references('id')->on('projects');
            $table->timestamps();
        });
    }
    
    public function down(){
        Schema::dropIfExists('project_sections');
    }
}
