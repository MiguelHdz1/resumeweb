<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NetworkTypes extends Migration
{
    public function up()
    {
        Schema::create('network_types',function(Blueprint $table){
            $table->increments('id')->nullable(false);
            $table->string('name',60);
            $table->string('icon',150);
            $table->char('status',1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('network_types');
    }
}
