<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{    
    public function up()
    {
        Schema::create('users', function(Blueprint $table){
            $table->increments('id')->nullable();
            $table->string('email',150)->nullable(false);
            $table->string('password',250)->nullable(false);
            $table->char('status',1)->nullable(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
