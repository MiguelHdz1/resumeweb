<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Projects extends Migration
{
    public function up()
    {
        Schema::create('projects',function(Blueprint $table){
            $table->increments('id')->nullable(false);
            $table->string('name',40)->nullable(false);
            $table->string('description',300)->nullable(false);
            $table->string('link',100)->nullable(true);
            $table->unsignedInteger('id_user')->nullable(false);
            $table->foreign('id_user')->references('id')->on('users')->nullable(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
