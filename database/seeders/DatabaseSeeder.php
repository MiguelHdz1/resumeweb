<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user= [['id'=>1,'type_user'=>1,'email'=>'luisramoshernandez27@hotmail.com','password'=> bcrypt('admin'),'status'=>'1']];
        \DB::table('users')->insert($user);
        $profile=[[
            'name'=>'Luis',
            'first_name'=>'Ramos',
            'last_name'=>'Hernandez',
            'gender'=>'M',
            'birthday'=>'2000-01-01 00:00:00',
            'phone_number'=>'5568983350',
            'avatar'=>'avatar.png',
            'background'=>'hack.jpg',
            'about_me'=>'I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.',
            'profession'=>'Human',
            'current_job'=>'working',
            'lives_in'=>'The moon',
            'id_user'=>1]];
        \DB::table('profiles')->insert($profile);
    }
}
