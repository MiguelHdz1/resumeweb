$(document).ready(function () {
    $('.collapsible').collapsible();
    var elem = document.querySelector('.collapsible.expandable');
    var instance = M.Collapsible.init(elem, {
        accordion: false
    });
    
    $('.datepicker').datepicker({
        minDate: new Date('01/01/1970'),        
        maxDate: new Date('12/31/2000'),
        defaultDate: new Date('12/31/2000')
    });

$(".livepreview").livePreview({
    trigger: 'hover',
    viewWidth: 300,  
    viewHeight: 200,  
    targetWidth: 1000,  
    targetHeight: 800,  
    scale: '0.5', 
    offset: 50,
    position: 'left'
});
});