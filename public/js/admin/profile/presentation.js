$(document).ready(function () {
    $('.tap-target').tapTarget();
    $('.tap-target').tapTarget('open');
    $('#message').on('click', function (e) {
        e.preventDefault();
        $('.tap-target').tapTarget('open');
    });

    $(document).on('change', '#file_back', function () {
        readURL(this, '#img_back');
    });
    $(document).on('change', '#file_profile', function () {
        readURL(this, '#img_profile');
    });


});

function readURL(input, id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(id).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}