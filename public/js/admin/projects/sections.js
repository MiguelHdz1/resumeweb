$(document).ready(function () {
    var asset = window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/';
    var path = $('.image_back').attr('data-bind');

    if (asset == path) {
        path = asset + 'images/project_default.jpg';
    }
    $('.tinted-image').css('background', 'linear-gradient(rgba(0, 0, 0, 0.7),rgba(0, 0, 0, .65)),url("' + path + '")');
    $('.tinted-image').css('background-position', 'center');
    $('.tinted-image').css('background-size', 'cover');
    $('.tinted-image').css('background-repeat', 'no-repeat');
});