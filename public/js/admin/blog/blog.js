$(document).ready(function () {
    $('.preview').fadeOut();
    $('.preview').html($('#blog_area').val());
    $('#btn-hide-preview').fadeOut();
    $('#blog_area').trumbowyg({
        lang: 'es',
        removeformatPasted: true
    }).on('tbwchange',function(){
        $('.preview').html($('.trumbowyg-editor').html());
        $('.materialboxed').materialbox();
    }).on('tbwpaste',function(){
        $('.preview').html($('.trumbowyg-editor').html());
        $('.materialboxed').materialbox();
    });
    
    $(document).on('click', '#btn-preview', function (evt) {
        evt.preventDefault();
        $(this).fadeOut(function(){
            $('#btn-hide-preview').fadeIn();
        });
        $('.preview').fadeIn();
        $('.materialboxed').materialbox();
    });
    
    $(document).on('click', '#btn-hide-preview', function (evt) {
        evt.preventDefault();
        $(this).fadeOut(function(){
            $('#btn-preview').fadeIn();
        });
        $('.preview').fadeOut();
    });       
});