$(document).ready(function () {
    $(document).on('click','#btn-delete',function(evt){
        evt.preventDefault();                
        $('#id_delete').val($(this).attr('data-bind'));
        $('#modal-delete').modal('open');
    });
    
    $(document).on('click','#btn-delete-accept',function(evt){
        evt.preventDefault();
        $('#form_delete').submit();        
    });
    
    var asset =window.location.protocol+'//'+window.location.hostname+':'+window.location.port+'/';    
    var path = $('.image_back').attr('data-bind');
    
    //if(asset != path){
    //    path = asset+'images/blog.jpg';
    //}    
    $('.tinted-image').css('background-position','center');
    $('.tinted-image').css('background-size','cover');
    $('.tinted-image').css('background-repeat','no-repeat');
});