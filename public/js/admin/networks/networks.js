$(document).ready(function (){    
    $(document).on('click','#btn-delete',function(evt){
        evt.preventDefault();                
        $('#id_delete').val($(this).attr('data-bind'));
        $('#modal-delete').modal('open');
    });
    
    $(document).on('click','#btn-delete-accept',function(evt){
        evt.preventDefault();
        $('#form_delete').submit();        
    });
});