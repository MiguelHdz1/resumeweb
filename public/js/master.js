$(document).ready(function () {    
    $('.sidenav').sidenav();
    $('.tooltipped').tooltip();
    $(".dropdown-trigger").dropdown();
    $('.modal').modal({
        outDuration: 500,
        dismissible: false
    });
    if ($('.message').attr('id') != null) {
        if ($('.message').attr('id') == 'msg-modal') {
            $('#modal-msg').modal('open');
        } else {
            M.toast({
                html: $('.message').attr('data-bind'),
                classes: $('.message').attr('id')
            });
        }
    }
    $('select').formSelect();
    $('textarea').characterCounter();
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        i18n: {
            cancel: 'Cancelar',
            months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
            weekdaysAbbrev: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
            selectMonths: true,
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Ok',
            labelMonthNext: 'Siguiente mes',
            labelMonthPrev: 'Mes anterior',
            labelMonthSelect: 'Selecciona un mes',
            labelYearSelect: 'Selecciona un año',
        }
    });    
    $('.materialboxed').materialbox();
});